QT += serialport

INCLUDEPATH = $$PWD/HDLC

HEADERS += \
    $$PWD/headers/Core.h \
    $$PWD/headers/Counter.h \
    $$PWD/headers/Enums.h \
    $$PWD/headers/Error.h \
    $$PWD/headers/io/iIO.h \
    $$PWD/sources/Frame.h \
    $$PWD/sources/Serializer.h \
    $$PWD/sources/Utils.h \
    $$PWD/sources/io/IOFactory.h \
    $$PWD/sources/io/iIO.h \
    $$PWD/sources/io/ioSerialPort.h \
    $$PWD/sources/io/ioSerialPortFactory.h

SOURCES += \
    $$PWD/sources/Core.cpp \
    $$PWD/sources/Counter.cpp \
    $$PWD/sources/Frame.cpp \
    $$PWD/sources/Serializer.cpp \
    $$PWD/sources/Utils.cpp \
    $$PWD/sources/io/ioSerialPort.cpp \
    $$PWD/sources/io/ioSerialPortFactory.cpp
