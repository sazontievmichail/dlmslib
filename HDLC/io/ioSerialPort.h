#pragma once
#ifndef IOSERIALPORT_H
#define IOSERIALPORT_H

#include "HDLC/io/iIO.h"
#include <QSerialPort>


namespace HDLC
{


class IOSerialPort:  public IIO {
    Q_OBJECT

    QSerialPort serial;
    IOSetting setting = defaultIOSetting;
    bool isInit = false;
public:
    explicit IOSerialPort(const IOSetting &setting = defaultIOSetting) noexcept;

    bool Open() noexcept;
    void Close(void) noexcept;

    bool IsOpen() noexcept;

    bool WriteData(const QByteArray &data) noexcept;
    bool ReadData(QByteArray &data) noexcept;

    QString SerialPortName() noexcept;

    ~IOSerialPort();
};

}

#endif // IOSERIALPORT_H
