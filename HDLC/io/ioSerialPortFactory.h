#pragma once
#ifndef IOSERIALPORTFACTORY_H
#define IOSERIALPORTFACTORY_H

#include "HDLC/io/IOFactory.h"
#include <QMap>



namespace HDLC
{


class IOSerialPortFactory : public IOFactory
{
private:
    static void DestroyIO(IIO* io);

public:
    IOSerialPortFactory();
    ~IOSerialPortFactory();

    QSharedPointer<IIO> CreateIO(const IOSetting &ioSettings);

private:
    static QMap<QString, QWeakPointer<IIO>> openIO;
};





}



#endif // IOSERIALPORTFACTORY_H
