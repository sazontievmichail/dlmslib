#include "HDLC/io/ioSerialPort.h"

namespace HDLC
{



IOSerialPort::IOSerialPort(const IOSetting &setting) noexcept {
    this->setting = setting;
    this->isInit = false;

    serial.setPortName(setting.portName);
    serial.setBaudRate((quint32)setting.boudRate);
    serial.setDataBits(QSerialPort::Data8);
    serial.setParity(QSerialPort::NoParity);
    serial.setStopBits(QSerialPort::OneStop);
}

bool IOSerialPort::Open() noexcept {
    if (!serial.isOpen()) {
        serial.close();
    }
    isInit = serial.open(QSerialPort::ReadWrite);
    return isInit;
}
bool IOSerialPort::IsOpen() noexcept {
    return serial.isOpen();
}


bool IOSerialPort::WriteData(const QByteArray &data) noexcept {
    if (!isInit) return false;

    serial.write(data);
    serial.waitForBytesWritten(setting.waitTimeOut);

    emit DataTransmit(data);
    return true;
}

bool IOSerialPort::ReadData(QByteArray &data) noexcept
{
    if (!isInit) {
        return false;
    }

    data.clear();
    data = serial.readAll();

    int attemptCount = 0;
    do {
        serial.waitForReadyRead(setting.waitTimeOut);
        do {
            data.append(serial.readAll());
        } while (serial.waitForReadyRead(setting.readTimeOut));

        if (data.size() < this->setting.minimalPackageSize || (char)0x7E != data[data.size() - 1]) {
            attemptCount ++;
        }
        else {
            break;
        }
    }  while (this->setting.attemptsWait > attemptCount);

    if (0 != data.size()) {
        emit DataReceive(data);
    }
    return true;
}
void IOSerialPort::Close(void) noexcept {
    if (serial.isOpen()) {
        serial.close();
    }
}

QString IOSerialPort::SerialPortName() noexcept {
    return setting.portName;
}

IOSerialPort::~IOSerialPort() {
    IOSerialPort::Close();
}


}
