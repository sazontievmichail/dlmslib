#include "HDLC/io/ioSerialPortFactory.h"
#include "HDLC/io/ioSerialPort.h"


namespace HDLC
{

QMap<QString, QWeakPointer<IIO>> IOSerialPortFactory::openIO = QMap<QString, QWeakPointer<IIO>>();


void IOSerialPortFactory::DestroyIO(IIO* io) {
    openIO.remove(((IOSerialPort*)io)->SerialPortName());
    delete io;
    io = nullptr;
}


IOSerialPortFactory::IOSerialPortFactory() {

};

IOSerialPortFactory::~IOSerialPortFactory() {

};

QSharedPointer<IIO> IOSerialPortFactory::CreateIO(const IOSetting &ioSettings)
{
    if (openIO.contains(ioSettings.portName)) {
        return openIO[ioSettings.portName].lock();
    }
    else {
        QSharedPointer<IIO> ioPtr(new IOSerialPort(ioSettings), DestroyIO);
        openIO[ioSettings.portName] = ioPtr.toWeakRef();
        return ioPtr;
    }
}

}



