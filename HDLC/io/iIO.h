#pragma once
#ifndef IIO_H
#define IIO_H

#include <QObject>
#include "HDLC/Enums.h"

namespace HDLC
{

class IIO : public QObject {
    Q_OBJECT
public:
    explicit IIO(QObject *parent = nullptr) : QObject(parent) {};
    /**
     * @author      Michail Sazontev
     * @brief       Open connection
     *
     * @param[in]   setting      The input/output connector settings
     *
     * @return      true if success
     */
    virtual bool Open() noexcept = 0;

    /**
     * @author      Michail Sazontev
     * @brief       Return interface status
     *
     * @return      Status, true if open
     */
    virtual bool IsOpen() noexcept = 0;

    /**
     * @author      Michail Sazontev
     * @brief       Write data to the interface
     *
     * @param[in]   data        Data required to send
     *
     * @return      Status, true if send success
     */
    virtual bool WriteData(const QByteArray &data) noexcept = 0;

    /**
     * @author      Michail Sazontev
     * @brief       Read data to the interface
     *
     * @param[out]   data       Еhe buffer into which the data is read
     *
     * @return      Status, true if read success
     */
    virtual bool ReadData(QByteArray &data) noexcept = 0;

    /**
     * @author      Michail Sazontev
     * @brief       Close interface
     */
    virtual void Close(void) noexcept = 0;

    virtual ~IIO(){};

signals:
    void DataReceive(const QByteArray &data);
    void DataTransmit(const QByteArray &data);
};



}


#endif // IIO_H
