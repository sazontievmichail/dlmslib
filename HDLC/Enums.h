#pragma once
#ifndef HDLCTYPES_H
#define HDLCTYPES_H

#include <QString>
#include <QByteArray>

namespace HDLC
{

enum class BaudRate : quint32 {
    Baud1200 = 1200,
    Baud2400 = 2400,
    Baud4800 = 4800,
    Baud9600 = 9600,
    Baud19200 = 19200,
    Baud38400 = 38400,
    Baud57600 = 57600,
    Baud115200 = 115200,
};

struct IOSetting {
    BaudRate boudRate;                      // Скорость передачи даных
    quint16 waitTimeOut;                    // Время ожидания чтения данных
    quint16 readTimeOut;                    // Время ожидания между посылками
    quint8 attemptsWait;                    // Число повторений ожидани сообщения
    quint8 minimalPackageSize;              // Минимальный размер поысылки

    QString portName;                       // Имя порта передачи данных
    quint8 attemptsConnection;              // Число попыток соединени / разъединени
    quint32 attemptsConnectionTimeout;      // интервал между попытками соединения / разединения (мСек)
};

struct ComunicationSetting {
    quint8 txWindowSize;            // Размер окна при передаче
    quint8 rxWindowSize;            // Размер окна при приеме
    quint16 txFrameSize;            // Размер посылки при передаче
    quint16 rxFrameSize;            // Размер посылки при приеме
};




const IOSetting defaultIOSetting = {.boudRate = BaudRate::Baud9600,
                                    .waitTimeOut = 200,
                                    .readTimeOut = 50,
                                    .attemptsWait = 10,
                                    .minimalPackageSize = 7,
                                    .portName = "",
                                    .attemptsConnection = 3,
                                    .attemptsConnectionTimeout = 1000};
const ComunicationSetting defaultHDLCComunicationSetting = { .txWindowSize = 1, .rxWindowSize = 1, .txFrameSize = 128, .rxFrameSize = 128};


}

#endif // HDLCTYPES_H
