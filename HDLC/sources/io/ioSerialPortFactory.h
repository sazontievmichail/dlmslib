#ifndef IOSERIALPORTFACTORY_H
#define IOSERIALPORTFACTORY_H

#include "IOFactory.h"
#include <QMap>

namespace HDLC {

class IOSerialPortFactory : public IOFactory {
public:
    IOSerialPortFactory();
    ~IOSerialPortFactory();

    QSharedPointer<IIO> createIO(const IOSetting &ioSettings);

private:
    static void destroyIO(IIO* io);

private:
    static QMap<QString, QWeakPointer<IIO>> openIO;
};

}
#endif // IOSERIALPORTFACTORY_H
