#include "ioSerialPortFactory.h"
#include "ioSerialPort.h"

namespace HDLC {

QMap<QString, QWeakPointer<IIO>> IOSerialPortFactory::openIO = QMap<QString, QWeakPointer<IIO>>();

void IOSerialPortFactory::destroyIO(IIO* io) {
    openIO.remove(((IOSerialPort*)io)->serialPortName());
    delete io;
    io = nullptr;
}

IOSerialPortFactory::IOSerialPortFactory() {

};

IOSerialPortFactory::~IOSerialPortFactory() {

};

QSharedPointer<IIO> IOSerialPortFactory::createIO(const IOSetting &ioSettings)
{
    if (openIO.contains(ioSettings.portName)) {
        return openIO[ioSettings.portName].lock();
    } else {
        QSharedPointer<IIO> ioPtr(new IOSerialPort(ioSettings), destroyIO);
        openIO[ioSettings.portName] = ioPtr.toWeakRef();
        return ioPtr;
    }
}

}



