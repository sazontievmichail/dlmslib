#ifndef IOFACTORY_H
#define IOFACTORY_H

#include "HDLC/headers/io/iIO.h"
#include <QSharedPointer>

namespace HDLC {

/**
 * @brief The Factory class needed to create io object
 */
class IOFactory {
public:
    /**
     * @brief The factory method, create io object and return smart pointer
     *
     * @param[in] ioSettings    The io initialie setting
     *
     * @return                  Smart pointer to io object
     */
    virtual QSharedPointer<IIO> createIO(const IOSetting &ioSettings) = 0;
};

}
#endif // IOFACTORY_H
