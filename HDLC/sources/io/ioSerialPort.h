#ifndef IOSERIALPORT_H
#define IOSERIALPORT_H

#include "HDLC/headers/io/iIO.h"
#include <QSerialPort>

namespace HDLC {

class IOSerialPort:  public IIO {
    Q_OBJECT

public:
    explicit IOSerialPort(const IOSetting &setting = defaultIOSetting) noexcept;
    ~IOSerialPort();

    bool open() noexcept;
    void close(void) noexcept;

    bool isOpen() noexcept;

    bool writeData(const QByteArray &data) noexcept;
    bool readData(QByteArray &data) noexcept;

    QString serialPortName() noexcept;

private:
    QSerialPort serial;
    IOSetting setting;
    bool isInit;
};

}
#endif // IOSERIALPORT_H
