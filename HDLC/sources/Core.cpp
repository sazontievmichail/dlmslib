#include "../headers/Core.h"
#include "Frame.h"
#include "io/ioSerialPortFactory.h"
#include "Serializer.h"
#include <QThread>

namespace HDLC {

Core::Core() noexcept {
    ioFactory = QSharedPointer<IOFactory>(new IOSerialPortFactory());
}

Core::~Core() noexcept {
    if (io) {
        io->close();
    }
}

void Core::openIO() {
    if (!io->isOpen()) {
        if (!io->open()) {
            throw Exception(Error::InterfaceNotOpen);
        }
    }
}

void Core::writePackage(Frame &frame) {
    openIO();

    if (Frame::DetailType::UNSET == frame.getType()) {
        throw Exception(Error::WrongPackageType);
    }
    frame.nr = nr;
    frame.ns = ns;
    QVector<Frame>frames = Serializer::dicollect(frame, comSetting);

    for (auto &fr : frames) {
        io->writeData(Serializer::serialize(fr));
        if (!fr.isComplete()) {
            // UNDONE Не реализован механизм получени/запроса неуспешно принтых пакетов
            Frame answer = readPackage();
            switch ((quint8)answer.getType()) {
                case (quint8)Frame::DetailType::RR:
                    break;
                case (quint8)Frame::DetailType::RNR:
                    throw Exception(Error::NotSupportRNR);
                    break;
                case (quint8)Frame::DetailType::REJ:
                    throw Exception(Error::DeviceRejected);
                    break;
                default:
                    throw Exception(Error::WrongPackageType);
                    break;
            }

            if ((answer.nr - 1) != fr.ns) {   // ПУ не имеет возможности отправить пропущенный кадр
                throw Exception(Error::SkippedFrame);
            }
        }
        if (Frame::DetailType::I == fr.getType()) {
            ns++;
        }
    }
}

Frame Core::readPackage() {
    openIO();

    QByteArray data;
    Frame frames = Frame(Frame::DetailType::UNSET);
    Frame frame = Frame(Frame::DetailType::UNSET);

    do {
        io->readData(data);
        if (0 == data.size()) {
            throw Exception(Error::NoAnswer);
        }
        Frame frame = Serializer::deserialize(data, Serializer::Direction::ServerToClient);
        // UNDONE Не реализован механизм получени/запроса неуспешно принтых пакетов
        switch ((quint8)frame.getType()) {
            case (quint8)Frame::DetailType::I:
                break;
            case (quint8)Frame::DetailType::RR:
            case (quint8)Frame::DetailType::RNR:
            case (quint8)Frame::DetailType::REJ:
            case (quint8)Frame::DetailType::UA:
            case (quint8)Frame::DetailType::SREJ:
                return frame;
            default:
                throw Exception(Error::WrongPackageType);
                break;
        }
        if (nr != frame.ns || ns != frame.nr) {
            throw Exception(Error::SkippedFrame);
        }
        nr++;
        if (!frames.glue(frame)) {
            throw Exception(Error::GlueError);
        }
        if (!frames.isComplete()) {
            frame.setType(Frame::DetailType::RR);
            frame.nr = ns;
            frame.ns = ns;
            frame.clearData();

            writePackage(frame);
        }
    } while (!frames.isComplete());

    return frames;
}

Error Core::open(const IOSetting ioSettings, const quint8 clientAddr, const quint16 logicalAddr, const quint16 physicalAddr) noexcept {
    io = ioFactory->createIO(ioSettings);

    this->ioSetting = ioSettings;
    this->clientAddr = clientAddr;
    this->logicalAddr = logicalAddr;
    this->physicalAddr = physicalAddr;
    try {
        bool res = true;
        openIO();

        Frame snrm = Frame(Frame::DetailType::SNRM, clientAddr, logicalAddr, physicalAddr);
        QByteArray ua;
        Frame uaF = Frame(Frame::DetailType::UNSET);
        int attemptCount = this->ioSetting.attemptsConnection;
        do {
            attemptCount--;
            res = io->writeData(Serializer::serialize(snrm));
            if (!res) {
                return Error::CantWriteData;
            }
            res = io->readData(ua);
            if (!res) {
                return Error::CantReadData;
            }
            uaF = Serializer::deserialize(ua, Serializer::Direction::ServerToClient);

            if (Frame::DetailType::UA == uaF.getType()) {
                break;
            } else if (Frame::DetailType::DM == uaF.getType()) {
                return Error::InterfaceNotOpen;
            } else {
                return Error::WrongPackageType;
            }
        } while (0 < attemptCount);

        nr = 0x00;
        ns = 0x00;
        //@TODO: Необходимо реализовать смену скорости соединения после установления связи
        comSetting =  Serializer::deserializeUA(uaF);
    }  catch (Exception &ex) {
        return ex.error;
    }
    return Error::Ok;
}

Error Core::transfer(const QByteArray &txData, QByteArray &rxData) noexcept {
    if (io.isNull()) {
        return Error::InterfaceNotOpen;
    }
    Frame frame = Frame(Frame::DetailType::I, clientAddr, logicalAddr, physicalAddr, txData);
    try {
        writePackage(frame);
        frame = readPackage();
    }  catch (Exception &ex) {
        return ex.error;
    }

    rxData.clear();
    rxData.append(frame.getData());

    return Error::Ok;
}

IIO* Core::getIO() noexcept {
    return io.get();
}

Error Core::close() noexcept {
    if (io.isNull()) {
        return Error::Ok;
    }
    Frame frame = Frame(Frame::DetailType::DISC, clientAddr, logicalAddr, physicalAddr);
    Frame answer = Frame(Frame::DetailType::UNSET);

    try {
        io->writeData(Serializer::serialize(frame));
        answer = readPackage();
    }  catch (...) { }
    io->close();

    return Error::Ok;
}

}
