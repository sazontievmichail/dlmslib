#include "Frame.h"

namespace HDLC {
namespace  {
const quint8 maxAddress1B = 0x7F;
const quint16 maxAddress2B = 0x3fff;
const quint8 endOfAddress = 0x01;
const quint8 addressMask = 0xFE;
}

Frame::Frame() noexcept {
    complete = true;
    nr = 0;
    ns = 0;
    pf = true;
    type = DetailType::UNSET;
}

Frame::Frame(const DetailType type) noexcept {
    complete = true;
    nr = 0;
    ns = 0;
    pf = true;
    this->type = type;
}

Frame::Frame(const DetailType type, const quint8 clientAddr, const quint16 logicalAddr, const quint16 physicalAddr) noexcept {
    complete = true;
    nr = 0;
    ns = 0;
    pf = true;
    this->type = type;
    setAddress(clientAddr, logicalAddr, physicalAddr);
}

Frame::Frame(const DetailType type, const quint8 clientAddr, const quint16 logicalAddr, const quint16 physicalAddr, const QByteArray &data) noexcept {
    complete = true;
    nr = 0;
    ns = 0;
    pf = true;
    this->type = type;
    setAddress(clientAddr, logicalAddr, physicalAddr);
    this->data.append(data);
}

bool Frame::setAddress(const quint8 clientAddr, const quint16 logicalAddr, const quint16 physicalAddr) noexcept {
    if (maxAddress1B < clientAddr
            || maxAddress2B < logicalAddr
            || maxAddress2B < physicalAddr) {
        return false;
    }

    this->clientAddr.clear();
    this->serverAddr.clear();

    this->clientAddr.push_back((clientAddr << 1)|endOfAddress);

    if (maxAddress1B >= logicalAddr && maxAddress1B >= physicalAddr) {
        this->serverAddr.push_back((logicalAddr << 1)&addressMask);
        this->serverAddr.push_back((physicalAddr << 1)|endOfAddress);
    } else {
        this->serverAddr.push_back((logicalAddr >> 6)&addressMask);
        this->serverAddr.push_back((logicalAddr << 1)&addressMask);
        this->serverAddr.push_back((physicalAddr >> 6)&addressMask);
        this->serverAddr.push_back((physicalAddr << 1)|endOfAddress);
    }
    return true;
}

quint32 Frame::getServerAddress() const noexcept {
    quint32 res = 0;
    if (4 == serverAddr.size()) {       // 4ех байтный адресс
        res = (serverAddr[0] << 24) | (serverAddr[1] << 16) | (serverAddr[2] << 8) | serverAddr[3];
    } else if (2 == serverAddr.size()) {  // 2ух байтный адресс
        res = (serverAddr[0] << 16) | serverAddr[1];
    } else if (1 == serverAddr.size()) {  // 1 байтный адресс
        res = serverAddr[0];
    }
    return res;
}

quint8 Frame::getClientAddress() const noexcept {
    if (0 < clientAddr.size()) {
        return clientAddr[0];
    } else {
        return 0;
    }
}

void Frame::setType(const Frame::DetailType type) noexcept {
    this->type = type;
}

Frame::DetailType Frame::getType() const noexcept {
    return type;
}

bool Frame::glue(const Frame &frame) noexcept {
    if (DetailType::UNSET != type
            && (type != frame.type || getServerAddress() != frame.getServerAddress() || getClientAddress() != frame.getClientAddress())) {
        return false;
    }
    clientAddr = frame.clientAddr;
    serverAddr = frame.serverAddr;
    data.append(frame.data);
    nr = frame.nr;
    ns = frame.ns;
    complete = frame.complete;
    return true;
}

const QByteArray& Frame::getData() const noexcept {
    return data;
}
void Frame::clearData() noexcept {
    data.clear();
}

bool Frame::isComplete() const noexcept {
    return complete;
}

}
