#include "HDLC/headers/Enums.h"
#include "Serializer.h"
#include "Utils.h"
#include <iostream>

namespace HDLC {
namespace  {
const quint8 rxFrameSizeFlag =  0x05;
const quint8 txFrameSizeFlag =  0x06;
const quint8 rxWindowSizeFlag =  0x07;
const quint8 txWindowSizeFlag =  0x08;
}

QByteArray Serializer::serialize(const Frame &frame, Direction dir) noexcept {
    QByteArray res;

    int hSize = frame.clientAddr.size() + frame.serverAddr.size() + 5;          // Размер заголовка без адреса
    int fSize = hSize + frame.data.size() + (frame.data.size() > 0 ? 2 : 0);

    //Формирование заголовка HDLC фрейма, см. DataExchangeModel
    quint16 format = 0xA000 | (frame.complete ? 0x0000 : 0x0800) | (fSize & 0x07ff);
    quint8 control = (quint8)frame.type;
    Frame::Type type = getFrameType((quint8)frame.type);
    if (type == Frame::Type::I) {
        control |= frame.ns.val() << 1 | frame.nr.val() << 5;
    }
    else if (type == Frame::Type::S) {
        control |= frame.nr.val() << 5;
    }
    control = frame.pf ? (control | 0x10) : (control & ~0x10);

    res << format;
    res << ((Direction::ClientToServer == dir) ? frame.serverAddr : frame.clientAddr);
    res << ((Direction::ClientToServer == dir) ? frame.clientAddr : frame.serverAddr);
    res << control;
    quint16 cr = qChecksum(res.data(), res.size());
    cr = ((cr >> 8) & 0x00ff ) ^ ((cr << 8) & 0xff00);  // Разворачиваем контрольную сумму
    res << cr;

    if (frame.data.size()) {
        res << frame.data;
        cr = qChecksum(res.data(), res.size());
        cr = ((cr >> 8) & 0x00ff ) ^ ((cr << 8) & 0xff00);  // Разворачиваем контрольную сумму
        res << cr;
    }
    res.insert(0, 0x7e);    // 0x7E - флаг начала/конца кадра
    res << quint8(0x7e);      // 0x7E - флаг начала/конца кадра

    return res;
}

Frame Serializer::deserialize(const QByteArray &data, Direction dir) noexcept {
    QByteArray::const_iterator it = data.begin();
    QByteArray::const_iterator end = data.end();

    if (9 > data.size() || 0x7E != ReadUINT8(it, data)) {   // 9 - минимально допустимый размер кадра, флаг конца кадра, 0x7E - флаг начала/конца кадра
        return Frame(Frame::DetailType::UNSET);
    }

    quint16 format = ReadUINT16(it, data);
    quint16 size = format & 0x07FF;             // Нужны только последние 13 бит
    if (data.size() < size + 2
            || 0x7E != *(data.begin() + size + 1)   // 0x7E - флаг начала/конца кадра
            || 0xA000 != (0xF000&format)) {         // 0xA - это hdlc
        return Frame(Frame::DetailType::UNSET);
    }
    QByteArray addr0 = parseAddress(it, data);
    it += addr0.size();
    QByteArray addr1 = parseAddress(it, data);
    it += addr1.size();
    quint8 control = ReadUINT8(it, data);
    ReadUINT16(it, data);   // crc head
    quint8 sizeHead = 3 + addr0.size() + addr1.size();

    QByteArray::const_iterator crcIt = data.cbegin() + size - 1;
    quint16 crc = ReadUINT16(crcIt, data);
    crc = crc << 8 | crc >> 8;
    quint16 crcCalc = qChecksum(data.data() + 1, size - 2);
    if (crc != crcCalc) {
        return Frame(Frame::DetailType::UNSET);
    }
    // Определем счетчики кадра. См. DataExchangeModel стр. 49
    bool pf = (control & 0x10) ? true : false;
    quint8 ns = (control >> 1) & 0x07;      // 0x07 маска счетчиков кадра
    quint8 nr = (control >> 5) & 0x07;
    Frame::DetailType type = getFrameDetailType(control);
    bool complete = (format & 0x0800) ? false : true;

    Frame frame;
    frame.clientAddr = (Direction::ClientToServer == dir) ? addr1 : addr0;
    frame.serverAddr = (Direction::ClientToServer == dir) ? addr0 : addr1;
    frame.pf = pf;
    frame.type = type;
    frame.complete = complete;
    if (0 < end - it) {
        frame.data = data.mid(it - data.begin(), size - sizeHead - 4);
    }

    switch(type) {
        case Frame::DetailType::I:
            frame.nr = nr;
            frame.ns = ns;
            return frame;
        case Frame::DetailType::REJ:
        case Frame::DetailType::RR:
        case Frame::DetailType::RNR:
        case Frame::DetailType::SREJ:
            frame.nr = nr;
            return frame;
        case Frame::DetailType::SABM:
        case Frame::DetailType::UA:
        case Frame::DetailType::DM:
        case Frame::DetailType::XID:
        case Frame::DetailType::SNRM:
        case Frame::DetailType::UP:
        case Frame::DetailType::SIM_RIM:
        case Frame::DetailType::DISC:
        case Frame::DetailType::RSET:
        case Frame::DetailType::FRMR:
            return frame;
        default:
            return Frame(Frame::DetailType::UNSET);
    }
}

QVector<Frame> Serializer::dicollect(const Frame &frame, const ComunicationSetting &setting) noexcept {
    QVector<Frame> res = QVector<Frame>();
    Frame partFrame;

    Counter ns = frame.ns;
    auto it = frame.data.begin();
    do {
        partFrame = Frame(frame);
        partFrame.data = frame.data.mid(it - frame.data.begin(), setting.txFrameSize);
        if ((it + setting.txFrameSize) < frame.data.end()) {
            partFrame.complete = false;
        } else {
            partFrame.complete = true;
        }
        partFrame.ns = ns;
        res.push_back(partFrame);
        ns++;
        it += setting.txFrameSize;
    } while (it < frame.data.end());
    return res;
}

ComunicationSetting Serializer::deserializeUA(const Frame &frame) noexcept {
    ComunicationSetting setting = defaultHDLCComunicationSetting;

    QByteArray::const_iterator it = frame.data.begin();
    if (0x81 != quint8(*it) || 0x80 != quint8(*++it)) {     // 0x81 0x80 - флаги кадра UA
        return setting;
    }

    while (it != frame.data.end()) {
        ++it;
        switch ((quint8)*it) {
            case rxFrameSizeFlag:
                setting.rxFrameSize = parseValue(++it);
                break;
            case txFrameSizeFlag:
                setting.txFrameSize = parseValue(++it);
                break;
            case rxWindowSizeFlag:
                setting.rxWindowSize = parseValue(++it);
                break;
            case txWindowSizeFlag:
                setting.txWindowSize = parseValue(++it);
                break;
            default:
                break;
        }
    }
    return setting;
}

quint32 Serializer::parseValue(QByteArray::const_iterator &it) noexcept {
    quint32 val = 0;
    int len = (quint8)*it;
    for (int i = 0; i < len; i++) {
        ++it;
        val = val << 8;
        val |= (quint8)*it;
    }
    return val;
}

QByteArray Serializer::parseAddress(QByteArray::const_iterator it, const QByteArray &data) noexcept {
    QByteArray address;
    int size = 0;
    for (; it != data.end(); ++it) {
        size++;
        address.push_back(*it);
        if ((*it) & 0x01) {     // 0x01 - флаг конца блока с адресом
            break;
        }
    }
    return address;
}

Frame::Type Serializer::getFrameType(const quint8 type) noexcept {
    if ((type & 0x01) == 0) {               // 0x01 - маска информационного кадра
        return Frame::Type::I;
    } else if ((type & 0x03) == 0x01) {     // 0x03 - маска кадра S
        return Frame::Type::S;
    } else {
        return Frame::Type::U;
    }
}

Frame::DetailType Serializer::getFrameDetailType(const quint8 control) noexcept {
    if ((control & 0x01) == 0) {        // 0x01 - маска информационного кадра
        return Frame::DetailType::I;
    } else if ((control & 0x03) == 0x01) {  // 0x03 - маска кадра S
        return static_cast<Frame::DetailType>(control & 0xF);   // 0xF - маска типа кадра S
    } else {
        return static_cast<Frame::DetailType>(control & ~0x10); // 0x10 - маска типа кадра U
    }
}

}
