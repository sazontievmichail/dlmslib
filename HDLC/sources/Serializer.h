#ifndef HDLCSERIALIZER_H
#define HDLCSERIALIZER_H

#include "Frame.h"
#include "HDLC/headers/Enums.h"
#include <QVector>
#include <QByteArray>
#include <QCoreApplication>

namespace HDLC {

class Serializer {
public:
    enum class Direction {
        ClientToServer,
        ServerToClient
    };

public:
    /**
     * @author      Michail Sazontev
     * @brief       Splits a frame into a sequence of bytes
     *
     * @param[in]   frame       The frame
     * @param[in]   dir         Package sending direction
     *
     * @return      Bytes array
     */
    static QByteArray serialize(const Frame &frame, Direction dir = Direction::ClientToServer) noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Collects a sequence of bytes into a frame
     *
     * @param[in]   data        The binary array
     * @param[in]   dir         Package sending direction
     *
     * @return      Bytes array
     */
    static Frame deserialize(const QByteArray &data, Direction dir = Direction::ClientToServer) noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Splits a frame into frames with the length specified in the settings
     *
     * @param[in]   frame       The frame
     * @param[in]   setting     Splits settings
     *
     * @return      Frames collection
     */
    static QVector<Frame> dicollect(const Frame &frame, const ComunicationSetting &setting = HDLC::defaultHDLCComunicationSetting) noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Deserialize HDLC UA frame
     *
     * @param[in]   data        The frame
     *
     * @return      structure of HDLC comunication setting
     */
    static ComunicationSetting deserializeUA(const Frame &data) noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Create HDLC SNRM frame
     *
     * @param[in]   setting     The HDLC protocol setting
     *
     * @return      HDLC frame
     */
    static Frame serializeSNRM(const ComunicationSetting &setting) noexcept;

private:
    /**
     * @author      Michail Sazontev
     * @brief       Parse HDLC value as uint. For example: "02 00 80" -> 128
     *
     * @param[in]   it          iterator pointing to the beginning of a value
     *
     * @return      uint value
     */
    static quint32 parseValue(QByteArray::const_iterator &it) noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Parse HDLC addres
     *
     * @param[in]   it          iterator pointing to the beginning of a value
     *
     * @return      address
     */
    static QByteArray parseAddress(QByteArray::const_iterator it, const QByteArray &data) noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Returns the verbose type of the frame
     *
     * @param[in]   control     control byte
     *
     * @return      type
     */
    static Frame::DetailType getFrameDetailType(const quint8 control) noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Returns type of the frame like as I, U, S
     *
     * @param[in]   type     frame type
     *
     * @return      HDLC frame type
     */
    static Frame::Type getFrameType(const quint8 type) noexcept;
};

}
#endif // HDLCSERIALIZER_H
