#include "HDLC/headers/Counter.h"

namespace HDLC {
namespace  {
const quint8 counterMask = 0x07;
}

Counter::Counter() {
    counter = 0;
}

Counter::Counter(const quint8 &val) {
    counter = val;
    counter &= counterMask;
}

quint8 Counter::val() const {
    return this->counter;
}

Counter& Counter::operator= (const quint8 &val) {
    this->counter = val;
    this->counter &= counterMask;
    return *this;
}

const Counter Counter::operator+ (const quint8 &val) {
    return Counter((this->counter + val) & counterMask);
}

const Counter Counter::operator- (const quint8 &val) {
    return Counter((this->counter - val) & counterMask);
}

bool operator== (const Counter &left, const Counter &right) {
    return (left.counter == right.counter);
}

bool operator!= (const Counter &left, const Counter &right) {
    return (left.counter != right.counter);
}

const Counter& operator++ (Counter& val) {
    val.counter++;
    val.counter &= counterMask;
    return val;
}

const Counter operator++ (Counter& i, int) {
    Counter oldValue(i.counter);
    i.counter++;
    i.counter &= counterMask;
    return oldValue;
}

const Counter& operator-- (Counter& val) {
    val.counter--;
    val.counter &= counterMask;
    return val;
}

const Counter operator-- (Counter& i, int) {
    Counter oldValue(i.counter);
    i.counter--;
    i.counter &= counterMask;
    return oldValue;
}

}
