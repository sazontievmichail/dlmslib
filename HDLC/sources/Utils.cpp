#include "HDLC/headers/Error.h"
#include "Utils.h"

QByteArray &operator<<(QByteArray &l, quint8 r) {
    l.append(r);
    return l;
}

QByteArray &operator<<(QByteArray &l, quint16 r) {
    return l << quint8(r >> 8) << quint8(r);
}

QByteArray &operator<<(QByteArray &l, quint32 r) {
    return l << quint16(r >> 16) << quint16(r);
}

QByteArray &operator<<(QByteArray &l, quint64 r) {
    return l << quint32(r >> 32) << quint32(r);
}

QByteArray &operator<<(QByteArray &l, QByteArray r) {
    l.append(r);
    return l;
}

quint8 ReadUINT8(QByteArray::const_iterator &it, const QByteArray &data) {
    quint8 res = 0;
   if (it < data.end()) {
       res = *it;
       it++;
       return res;
   } else {
       throw HDLC::Error::InvalidSize;
   }
}

quint16 ReadUINT16(QByteArray::const_iterator &it, const QByteArray &data) {
    return ReadUINT8(it, data) << 8 | ReadUINT8(it, data);
}

quint32 ReadUINT32(QByteArray::const_iterator &it, const QByteArray &data) {
    return ReadUINT16(it, data) << 16 | ReadUINT16(it, data);
}

quint64 ReadUINT64(QByteArray::const_iterator &it, const QByteArray &data) {
    return (quint64)ReadUINT32(it, data) << 32 | ReadUINT32(it, data);
}


