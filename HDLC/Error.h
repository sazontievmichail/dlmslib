#pragma once
#ifndef ERROR_H
#define ERROR_H

#include <QCoreApplication>



namespace HDLC
{


enum class Error : quint8 {
    Ok                  = 0 ,
    InterfaceNotOpen        ,
    CantReadData            ,
    CantWriteData           ,
    WrongPackageType        ,
    DeviceRejected          ,
    NoAnswer                ,
    IncorrectServerAddress  ,
    SkippedFrame            ,
    GlueError               ,
    ConnectionExist         ,
    IncorrectServerState    ,
    NotSupportRNR           , // На данный момент счетчик не поодерживает
    InvalidSize             ,
};


struct Exception {
    Error error;

    Exception(Error error){
        this->error = error;
    }
};


}


#endif // ERROR_H
