#pragma once
#ifndef HDLCFRAME_H
#define HDLCFRAME_H

#include <QByteArray>

#include "HDLC/src/Counter.h"


namespace HDLC
{

class Frame {
private:

    /**
     * @author      Michail Sazontev
     * @brief       Constructs HDLC frame
     */
    Frame() noexcept;
public:
    enum class Type : quint8 {
        I = 0x00,
        S = 0x01,
        U = 0x03
    };
    enum class DetailType : quint8 {
        I       = 0b00000000,

        RR      = 0b00000001,
        RNR     = 0b00001001,
        REJ     = 0b00000101,
        SREJ    = 0b00001101,

        UI      = 0b00000011,
        SNRM    = 0b10000011,
        DISC    = 0b01000011,
        UP      = 0b00100011,
        UA      = 0b01100011,
        TEST    = 0b11100011,
        SIM_RIM = 0b00000111,
        FRMR    = 0b10000111,
        DM      = 0b00001111,
        RSET    = 0b10001111,
        SARME   = 0b01001111,
        SNRME   = 0b11001111,
        SABM    = 0b00101111,
        XID     = 0b10101111,
        SABME   = 0b01101111,

        UNSET = 0xFF,
    };

    /**
     * @author      Michail Sazontev
     * @brief       Constructs HDLC frame
     *
     * @param[in]   type        The type of frame
     */
    Frame(const DetailType type) noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Constructs HDLC frame
     *
     * @param[in]   type            The type of frame
     * @param[in]   clientAddr      The HDLC client address
     * @param[in]   logicalAddr     The HDLC server logical addres
     * @param[in]   physicalAddr    The HDLC server physycal addres
     */
    Frame(const DetailType type, const quint8 clientAddr, const quint16 logicalAddr, const quint16 physicalAddr) noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Constructs HDLC frame
     *
     * @param[in]   type            The type of frame
     * @param[in]   clientAddr      The HDLC client address
     * @param[in]   logicalAddr     The HDLC server logical addres
     * @param[in]   physicalAddr    The HDLC server physycal addres
     * @param[in]   data            The HDLC data
     */
    Frame(const DetailType type, const quint8 clientAddr, const quint16 logicalAddr, const quint16 physicalAddr, const QByteArray &data) noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Set HDLC address
     *
     * @param[in]   type            The type of frame
     * @param[in]   clientAddr      The HDLC client address
     * @param[in]   logicalAddr     The HDLC server logical addres
     * @param[in]   physicalAddr    The HDLC server physycal addres
     *
     * @return      true, if new addresses is correct
     */
    bool SetAddress(const quint8 clientAddr, const quint16 logicalAddr, const quint16 physicalAddr) noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Return HDLC server address
     *
     * @return      HDLC server address (logical and physical)
     */
    quint32 GetServerAddress() const noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Return HDLC client address
     *
     * @return      HDLC client address
     */
    quint8 GetClientAddress() const noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Install type of HDLC frame
     *
     * @param[in]   type        The type of frame
     */
    void SetType(const DetailType type) noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Return HDLC frame type
     *
     * @return      HDLC frame type
     */
    DetailType GetType() const noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Depending on usage, indicates whether the package is last or complete
     *
     * @return      complete package
     */
    bool IsComplete() const noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Return package data
     *
     * @return      package data
     */
    const QByteArray& GetData() noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Clear package data
     */
    void ClearData() noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Glues two frames if they have the same type and address. Frame counters are not checked
     *
     * @param       frame       The frame to be glued
     *
     * @return      true if package has been glu
     */
    bool Glue(const Frame &frame) noexcept;



public:
    Counter ns;
    Counter nr;

protected:
    QByteArray _clientAddr;
    QByteArray _serverAddr;

    bool _pf;
    bool _complete;
    DetailType _type;

    QByteArray _data;

    friend class Serializer;
};

}

#endif // HDLCFRAME_H
