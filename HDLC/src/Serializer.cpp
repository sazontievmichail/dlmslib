#include "HDLC/src/Serializer.h"
#include "HDLC/HDLCUtils.h"

#include <iostream>

#define FRAME_MIN_SIZE 6;


namespace HDLC
{



QByteArray Serializer::Serialize(const Frame &frame, Direction dir) noexcept {
    QByteArray res;

    int hSize = frame._clientAddr.size() + frame._serverAddr.size() + 5;
    int fSize = hSize + frame._data.size() + (frame._data.size() > 0 ? 2 : 0);

    //Header
    quint16 format = 0xA000 | (frame._complete ? 0x0000 : 0x0800) | (fSize & 0x07ff);
    quint8 control = (quint8)frame._type;
    Frame::Type type = GetFrameType((quint8)frame._type);
    if (type == Frame::Type::I) {
        control |= frame.ns.val() << 1 | frame.nr.val() << 5;
    }
    else if (type == Frame::Type::S) {
        control |= frame.nr.val() << 5;
    }
    control = frame._pf ? (control | 0x10) : (control & ~0x10);

    res<<format;
    res<<((Direction::ClientToServer == dir) ? frame._serverAddr : frame._clientAddr);
    res<<((Direction::ClientToServer == dir) ? frame._clientAddr : frame._serverAddr);
    res<<control;
    quint16 cr = qChecksum(res.data(), res.size());
    cr = ((cr >> 8) & 0x00ff ) ^ ((cr << 8) & 0xff00);
    res<<cr;

    if (frame._data.size()) {
        res<<frame._data;
        cr = qChecksum(res.data(), res.size());
        cr = ((cr >> 8) & 0x00ff ) ^ ((cr << 8) & 0xff00);
        res<<cr;
    }
    res.insert(0, 0x7e);
    res<<quint8(0x7e);

    return res;
}
Frame Serializer::Deserialize(const QByteArray &data, Direction dir) noexcept
{
    QByteArray::const_iterator it = data.begin();
    QByteArray::const_iterator end = data.end();


    if (9 > data.size() || 0x7E != ReadUINT8(it, data)) {
        return Frame(Frame::DetailType::UNSET);
    }

    quint16 format = ReadUINT16(it, data);
    quint16 size = format & 0x07FF;
    if (data.size() < size + 2
            || 0x7E != *(data.begin() + size + 1)
            || 0xA000 != (0xF000&format)) {
        return Frame(Frame::DetailType::UNSET);
    }
    QByteArray addr0 = ParseAddress(it, data);
    it += addr0.size();
    QByteArray addr1 = ParseAddress(it, data);
    it += addr1.size();
    quint8 control = ReadUINT8(it, data);
    ReadUINT16(it, data);   // crc head
    quint8 sizeHead = 3 + addr0.size() + addr1.size();

    QByteArray::const_iterator crcIt = data.cbegin() + size - 1;
    quint16 crc = ReadUINT16(crcIt, data);
    crc = crc << 8 | crc >> 8;
    quint16 crcCalc = qChecksum(data.data() + 1, size - 2);
    if (crc != crcCalc) {
        return Frame(Frame::DetailType::UNSET);
    }

    bool pf = (control & 0x10) ? true : false;
    quint8 ns = (control >> 1) & 0x07;
    quint8 nr = (control >> 5) & 0x07;
    Frame::DetailType type = GetFrameDetailType(control);
    bool complete = (format & 0x0800) ? false : true;

    Frame frame;
    frame._clientAddr = (Direction::ClientToServer == dir) ? addr1 : addr0;
    frame._serverAddr = (Direction::ClientToServer == dir) ? addr0 : addr1;
    frame._pf = pf;
    frame._type = type;
    frame._complete = complete;
    if (0 < end - it) {
        frame._data = data.mid(it - data.begin(), size - sizeHead - 4);
    }

    switch(type) {
    case Frame::DetailType::I:
        frame.nr = nr;
        frame.ns = ns;
        return frame;
    case Frame::DetailType::REJ:
    case Frame::DetailType::RR:
    case Frame::DetailType::RNR:
    case Frame::DetailType::SREJ:
        frame.nr = nr;
        return frame;
    case Frame::DetailType::SABM:
    case Frame::DetailType::UA:
    case Frame::DetailType::DM:
    case Frame::DetailType::XID:
    case Frame::DetailType::SNRM:
    case Frame::DetailType::UP:
    case Frame::DetailType::SIM_RIM:
    case Frame::DetailType::DISC:
    case Frame::DetailType::RSET:
    case Frame::DetailType::FRMR:
        return frame;
    default:
        return Frame(Frame::DetailType::UNSET);
    }
}


QVector<Frame> Serializer::Dicollect(const Frame &frame, const ComunicationSetting &setting) noexcept {
    QVector<Frame> res = QVector<Frame>();
    Frame partFrame;

    Counter ns = frame.ns;
    auto it = frame._data.begin();
    do {
        partFrame = Frame(frame);
        partFrame._data = frame._data.mid(it - frame._data.begin(), setting.txFrameSize);
        if((it + setting.txFrameSize) < frame._data.end()){
            partFrame._complete = false;
        } else {
            partFrame._complete = true;
        }
        partFrame.ns = ns;
        res.push_back(partFrame);
        ns++;
        it += setting.txFrameSize;
    } while (it < frame._data.end());

    return res;
}

ComunicationSetting Serializer::DeserializeUA(const Frame &frame) noexcept {
    ComunicationSetting setting = defaultHDLCComunicationSetting;

    QByteArray::const_iterator it = frame._data.begin();
    if (0x81 != quint8(*it) || 0x80 != quint8(*++it)) {
        return setting;
    }
    while (it != frame._data.end()) {
        ++it;
        switch ((quint8)*it) {
        case 0x05:
            setting.rxFrameSize = ParseValue(++it);
            break;
        case 0x06:
            setting.txFrameSize = ParseValue(++it);
            break;
        case 0x07:
            setting.rxWindowSize = ParseValue(++it);
            break;
        case 0x08:
            setting.txWindowSize = ParseValue(++it);
            break;
        default:
            break;
        }
    }

    return setting;
}

quint32 Serializer::ParseValue(QByteArray::const_iterator &it) noexcept
{
    quint32 val = 0;
    int len = (quint8)*it;
    for (int i = 0; i < len; i++) {
        ++it;
        val = val << 8;
        val |= (quint8)*it;
    }


    return val;
}

QByteArray Serializer::ParseAddress(QByteArray::const_iterator it, const QByteArray &data) noexcept
{
    QByteArray address;
    int size = 0;

    for (; it != data.end(); ++it) {
        size++;
        address.push_back(*it);

        if ((*it) & 0x01) {
            break;
        }
    }

    return address;
}
Frame::Type Serializer::GetFrameType(const quint8 type) noexcept
{
    if ((type & 0x01) == 0) {
        return Frame::Type::I;
    }
    else if ((type & 0x03) == 0x01) {
        return Frame::Type::S;
    }
    else {
        return Frame::Type::U;
    }
}
Frame::DetailType Serializer::GetFrameDetailType(const quint8 control) noexcept
{
    if ((control & 0x01) == 0) {
        return Frame::DetailType::I;
    }
    else if ((control & 0x03) == 0x01) {
        return static_cast<Frame::DetailType>(control & 0xF);
    }
    else {
        return static_cast<Frame::DetailType>(control & ~0x10);
    }
}




}
