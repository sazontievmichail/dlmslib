#include "HDLC/src/Frame.h"

namespace HDLC
{


Frame::Frame() noexcept {
    this->_complete = true;
    this->nr = 0;
    this->ns = 0;
    this->_pf = true;
    this->_type = DetailType::UNSET;
}


Frame::Frame(const DetailType type) noexcept {
    Frame();
    this->_type = type;
}

Frame::Frame(const DetailType type, const quint8 clientAddr, const quint16 logicalAddr, const quint16 physicalAddr) noexcept {
    Frame();
    this->_type = type;
    SetAddress(clientAddr, logicalAddr, physicalAddr);
    this->_data.append(_data);
}

Frame::Frame(const DetailType type, const quint8 clientAddr, const quint16 logicalAddr, const quint16 physicalAddr, const QByteArray &data) noexcept {
    Frame();
    this->_type = type;
    SetAddress(clientAddr, logicalAddr, physicalAddr);
    this->_data.append(data);
}

bool Frame::SetAddress(const quint8 clientAddr, const quint16 logicalAddr, const quint16 physicalAddr) noexcept {
    if (0x7f < clientAddr
            || 0x3fff < logicalAddr
            || 0x3fff < physicalAddr) {
        return false;
    }

    this->_clientAddr.clear();
    this->_serverAddr.clear();

    this->_clientAddr.push_back( (clientAddr << 1)|0x01);

    if (0x7f >= logicalAddr && 0x7f >= physicalAddr) {
        this->_serverAddr.push_back((logicalAddr << 1)&0xFE);
        this->_serverAddr.push_back((physicalAddr << 1)|0x01);
    }
    else {
        this->_serverAddr.push_back((logicalAddr >> 6)&0xFE);
        this->_serverAddr.push_back((logicalAddr << 1)&0xFE);
        this->_serverAddr.push_back((physicalAddr >> 6)&0xFE);
        this->_serverAddr.push_back((physicalAddr << 1)|0x01);
    }

    return true;
}

quint32 Frame::GetServerAddress() const noexcept {
    quint32 res = 0;
    if (4 == this->_serverAddr.size()) {
        res = (this->_serverAddr[0] << 24) | (this->_serverAddr[1] << 16) | (this->_serverAddr[2] << 8) | this->_serverAddr[3];
    }
    else if (2 == this->_serverAddr.size()) {
        res = (this->_serverAddr[0] << 16) | this->_serverAddr[1];
    }
    else if (1 == this->_serverAddr.size()) {
        res = this->_serverAddr[0];
    }

    return res;
}

quint8 Frame::GetClientAddress() const noexcept {
    if (0 < this->_clientAddr.size()) {
        return _clientAddr[0];
    }
    else {
        return 0;
    }
}

void Frame::SetType(const Frame::DetailType type) noexcept {
    this->_type = type;
}

Frame::DetailType Frame::GetType() const noexcept {
    return this->_type;
}

bool Frame::Glue(const Frame &frame) noexcept {
    if (DetailType::UNSET != this->_type
            && (this->_type != frame._type || this->GetServerAddress() != frame.GetServerAddress() || this->GetClientAddress() != frame.GetClientAddress())) {
        return false;
    }

    this->_clientAddr = frame._clientAddr;
    this->_serverAddr = frame._serverAddr;
    this->_data.append(frame._data);
    this->nr = frame.nr;
    this->ns = frame.ns;
    this->_complete = frame._complete;

    return true;
}

const QByteArray& Frame::GetData() noexcept {
    return this->_data;
}
void Frame::ClearData() noexcept {
    this->_data.clear();
}

bool Frame::IsComplete() const noexcept {
    return this->_complete;
}


}
