#pragma once
#ifndef HDLCCOUNTER_H
#define HDLCCOUNTER_H

#include <QCoreApplication>

namespace HDLC
{


class Counter {
public:
    Counter() {
        counter = 0;
    }
    Counter(const quint8 &val) {
        counter = val;
        counter &= 0x07;
    }
    quint8 val() const {
        return this->counter;
    }

    friend const Counter& operator++ (Counter& val) {
        val.counter ++;
        val.counter &= 0x07;
        return val;
    }
    friend const Counter operator++ (Counter& i, int) {
        Counter oldValue(i.counter);
        i.counter++;
        i.counter &= 0x07;
        return oldValue;
    }
    friend const Counter& operator-- (Counter& val) {
        val.counter--;
        val.counter &= 0x07;
        return val;
    }
    friend const Counter operator-- (Counter& i, int) {
        Counter oldValue(i.counter);
        i.counter--;
        i.counter &= 0x07;
        return oldValue;
    }

    Counter& operator= (const quint8 &val) {
        this->counter = val;
        this->counter &= 0x07;
        return *this;
    }

    friend bool operator== (const Counter &left, const Counter &right) {
        return (left.counter == right.counter);
    }
    friend bool operator!= (const Counter &left, const Counter &right) {
        return (left.counter != right.counter);
    }

    const Counter operator+ (const quint8 &val) {
        return Counter((this->counter + val) & 0x07);
    }
    const Counter operator- (const quint8 &val) {
        return Counter((this->counter - val) & 0x07);
    }

private:
    quint8 counter;
};


}

#endif // HDLCCOUNTER_H
