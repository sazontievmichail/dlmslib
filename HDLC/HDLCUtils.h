#pragma once
#ifndef UTILS_H
#define UTILS_H

#include <QCoreApplication>
#include <QByteArray>


namespace HDLC
{


QByteArray &operator<<(QByteArray &l, quint8 r);
QByteArray &operator<<(QByteArray &l, quint16 r);
QByteArray &operator<<(QByteArray &l, quint32 r);
QByteArray &operator<<(QByteArray &l, quint64 r);
QByteArray &operator<<(QByteArray &l, QByteArray r);


quint8  ReadUINT8(QByteArray::const_iterator &it, const QByteArray &data);
quint16 ReadUINT16(QByteArray::const_iterator &it, const QByteArray &data);
quint32 ReadUINT32(QByteArray::const_iterator &it, const QByteArray &data);
quint64 ReadUINT64(QByteArray::const_iterator &it, const QByteArray &data);



}

#endif // UTILS_H
