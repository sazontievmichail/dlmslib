#include "HDLCCore.h"
#include "src/Frame.h"
#include "io/ioSerialPortFactory.h"

#include <QThread>

namespace HDLC
{


void HDLCCore::OpenIO()
{
    if(false == this->_io->IsOpen()){
        if(false == _io->Open()){
            throw Exception(Error::InterfaceNotOpen);
        }
    }
}


void HDLCCore::WritePackage(Frame &frame)
{
    OpenIO();

    if(Frame::DetailType::UNSET == frame.GetType()){
        throw Exception(Error::WrongPackageType);
    }
    frame.nr = nr;
    frame.ns = ns;
    QVector<Frame>frames = Serializer::Dicollect(frame, comSetting);


    for(auto &fr : frames)
    {
        _io->WriteData(Serializer::Serialize(fr));

        if(false == fr.IsComplete())
        {
            // UNDONE Не реализован механизм получени/запроса неуспешно принтых пакетов
            Frame answer = ReadPackage();
            switch ((quint8)answer.GetType())
            {
            case (quint8)Frame::DetailType::RR:
                break;
            case (quint8)Frame::DetailType::RNR:
                throw Exception(Error::NotSupportRNR);
                break;
            case (quint8)Frame::DetailType::REJ:
                throw Exception(Error::DeviceRejected);
                break;
            default:
                throw Exception(Error::WrongPackageType);
                break;
            }

            if((answer.nr - 1) != fr.ns)                            // ПУ не имеет возможности отправить пропущенный кадр
            {
                throw Exception(Error::SkippedFrame);
            }
        }

        if( Frame::DetailType::I == fr.GetType())
        {
            ns ++;
        }

    }
}
Frame HDLCCore::ReadPackage()
{
    OpenIO();

    QByteArray data;

    Frame frames = Frame(Frame::DetailType::UNSET);
    Frame frame = Frame(Frame::DetailType::UNSET);


    do {
        this->_io->ReadData(data);
        if (0 == data.size()) {
            throw Exception(Error::NoAnswer);
        }

        Frame frame = Serializer::Deserialize(data, Serializer::Direction::ServerToClient);

        // UNDONE Не реализован механизм получени/запроса неуспешно принтых пакетов
        switch ((quint8)frame.GetType()) {
        case (quint8)Frame::DetailType::I:
            break;
        case (quint8)Frame::DetailType::RR:
        case (quint8)Frame::DetailType::RNR:
        case (quint8)Frame::DetailType::REJ:
        case (quint8)Frame::DetailType::UA:
        case (quint8)Frame::DetailType::SREJ:
            if (Frame::DetailType::UNSET == frames.GetType()) {
                return frame;
            }
        default:
            throw Exception(Error::WrongPackageType);
            break;
        }

        if (nr != frame.ns || ns != frame.nr) {
            throw Exception(Error::SkippedFrame);
        }
        nr++;


        if (!frames.Glue(frame)) {
            throw Exception(Error::GlueError);
        }

        if(false == frames.IsComplete()) {
            frame.SetType(Frame::DetailType::RR);
            frame.nr = ns;
            frame.ns = ns;
            frame.ClearData();

            WritePackage(frame);
        }
    } while (false == frames.IsComplete());


    return frames;
}

Error HDLCCore::Open(const IOSetting ioSettings, const quint8 clientAddr, const quint16 logicalAddr, const quint16 physicalAddr) noexcept
{
    _io = ioFactory->CreateIO(ioSettings);

    this->ioSetting = ioSettings;
    this->clientAddr = clientAddr;
    this->logicalAddr = logicalAddr;
    this->physicalAddr = physicalAddr;
    try {
        bool res = true;
        OpenIO();

        Frame snrm = Frame(Frame::DetailType::SNRM, clientAddr, logicalAddr, physicalAddr);

        QByteArray ua;
        Frame uaF = Frame(Frame::DetailType::UNSET);
        int attemptCount = this->ioSetting.attemptsConnection;

        do {
            attemptCount--;

            res = _io->WriteData(Serializer::Serialize(snrm));
            if(false == res) {
                return Error::CantWriteData;
            }

            res = _io->ReadData(ua);
            if(false == res) {
                return Error::CantReadData;
            }
            uaF = Serializer::Deserialize(ua, Serializer::Direction::ServerToClient);


            if (Frame::DetailType::UA == uaF.GetType()) {
                break;
            } else if (Frame::DetailType::DM == uaF.GetType()) {
                return Error::InterfaceNotOpen;
            } else {
                return Error::WrongPackageType;
            }
        } while(0 < attemptCount);

        nr = 0x00;
        ns = 0x00;
        // TODO Необходимо реализовать смену скорости соединения после установления связи
        comSetting =  Serializer::DeserializeUA(uaF);

        return Error::Ok;

    } catch (Exception &ex)
    {
        return ex.error;
    }
}
Error HDLCCore::Transfer(const QByteArray &txData, QByteArray &rxData) noexcept
{
    if (true == _io.isNull()) {
        return Error::InterfaceNotOpen;
    }
    Frame frame = Frame(Frame::DetailType::I, clientAddr, logicalAddr, physicalAddr, txData);
    try
    {
        WritePackage(frame);
        frame = ReadPackage();
    }  catch (Exception &ex)
    {
        return ex.error;
    }

    rxData.clear();
    rxData.append(frame.GetData());

    return Error::Ok;
}
IIO* HDLCCore::GetIO() noexcept
{
    return _io.get();
}
Error HDLCCore::Close() noexcept
{
    if (true == _io.isNull()) {
        return Error::Ok;
    }
    Frame frame = Frame(Frame::DetailType::DISC, clientAddr, logicalAddr, physicalAddr);
    Frame answer = Frame(Frame::DetailType::UNSET);

    try {
        _io->WriteData(Serializer::Serialize(frame));
        answer = ReadPackage();
    }  catch (...) { }
    this->_io->Close();

    return Error::Ok;
}

HDLCCore::HDLCCore() noexcept
{
    ioFactory = QSharedPointer<IOFactory>(new IOSerialPortFactory());
}
HDLCCore::~HDLCCore() noexcept
{
    if (nullptr != _io)
        _io->Close();
}

}
