#pragma once
#ifndef HDLCCLIENT_H
#define HDLCCLIENT_H

#include "io/iIO.h"
#include "io/IOFactory.h"
#include "Enums.h"
#include "Error.h"
#include "src/Serializer.h"
#include <QSharedPointer>


namespace  HDLC
{


class HDLCCore {
private:

    /**
     * @brief OpenIO
     * @private
     */
    void OpenIO();
    /**
     * @brief WritePackage
     * @private
     */
    void WritePackage(Frame &frame);
    /**
     * @brief ReadPackage
     * @private
     */
    Frame ReadPackage();
public:

    /**
     * @author      Michail Sazontev
     * @brief       Constructs HDLC Client
     */
    HDLCCore() noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Open new HDLC connection
     *
     * @param[in]   ioSettings      The input/output connector settings
     * @param[in]   clientAddr      The HDLC client address
     * @param[in]   logicalAddr     The HDLC server logical addres
     * @param[in]   physicalAddr    The HDLC server physycal addres
     *
     * @return      return error code
     */
    Error Open(const IOSetting ioSettings, quint8 clientAddr, quint16 logicalAddr, quint16 physicalAddr) noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Sending data to the server and receiving a response
     *
     * @param[in]   txData          Binary data send to server
     * @param[out]  rxData          Binary data received from the server
     *
     * @return      return error code
     */
    Error Transfer(const QByteArray &txData, QByteArray &rxData) noexcept;

    /**
     * @author      Michail Sazontev
     * @brief GetIO
     *
     * @return const link on io object
     */
    IIO* GetIO() noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Close HDLC connection
     *
     * @return      return error code
     */
    Error Close() noexcept;


    ~HDLCCore() noexcept;

private:
    QSharedPointer<IOFactory> ioFactory;
    QSharedPointer<IIO> _io;

    Counter nr = 0;
    Counter ns = 0;
    ComunicationSetting comSetting;
    IOSetting ioSetting;

    quint8 clientAddr;
    quint16 logicalAddr;
    quint16 physicalAddr;
};


};

#endif // HDLCCLIENT_H
