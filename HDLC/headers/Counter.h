#ifndef HDLCCOUNTER_H
#define HDLCCOUNTER_H

#include <QCoreApplication>

namespace HDLC {

class Counter {
public:
    Counter();
    Counter(const quint8 &val);

    quint8 val() const;

    Counter& operator= (const quint8 &val);

    const Counter operator+ (const quint8 &val);
    const Counter operator- (const quint8 &val);

    friend bool operator== (const Counter &left, const Counter &right);
    friend bool operator!= (const Counter &left, const Counter &right);
    friend const Counter& operator++ (Counter& val);
    friend const Counter operator++ (Counter& i, int);
    friend const Counter& operator-- (Counter& val);
    friend const Counter operator-- (Counter& i, int);

private:
    quint8 counter;
};


}

#endif // HDLCCOUNTER_H
