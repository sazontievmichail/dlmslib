#ifndef HDLCCORE_H
#define HDLCCORE_H

#include "Enums.h"
#include "Error.h"
#include "io/iIO.h"
#include "Counter.h"
#include <QSharedPointer>

namespace  HDLC {

class IOFactory;

class Frame;

class Core {
public:
    /**
     * @author      Michail Sazontev
     * @brief       Constructs HDLC Client
     */
    Core() noexcept;
    ~Core() noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       GetIO
     *
     * @return const link on io object
     */
    IIO* getIO() noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Open new HDLC connection
     *
     * @param[in]   ioSettings      The input/output connector settings
     * @param[in]   clientAddr      The HDLC client address
     * @param[in]   logicalAddr     The HDLC server logical addres
     * @param[in]   physicalAddr    The HDLC server physycal addres
     *
     * @return      return error code
     */
    Error open(const IOSetting ioSettings, quint8 clientAddr, quint16 logicalAddr, quint16 physicalAddr) noexcept;
    /**
     * @author      Michail Sazontev
     * @brief       Close HDLC connection
     *
     * @return      return error code
     */
    Error close() noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Sending data to the server and receiving a response
     *
     * @param[in]   txData          Binary data send to server
     * @param[out]  rxData          Binary data received from the server
     *
     * @return      return error code
     */
    Error transfer(const QByteArray &txData, QByteArray &rxData) noexcept;

private:
    /**
     * @brief OpenIO
     * @private
     */
    void openIO();
    /**
     * @brief WritePackage
     * @private
     */
    void writePackage(Frame &frame);
    /**
     * @brief ReadPackage
     * @private
     */
    Frame readPackage();

private:
    QSharedPointer<IOFactory> ioFactory;
    QSharedPointer<IIO> io;

    Counter nr;
    Counter ns;
    ComunicationSetting comSetting;
    IOSetting ioSetting;

    quint8 clientAddr;
    quint16 logicalAddr;
    quint16 physicalAddr;
};

};

#endif // HDLCCORE_H
