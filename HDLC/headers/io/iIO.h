#ifndef IIO_H
#define IIO_H

#include "../Enums.h"
#include <QObject>

namespace HDLC {

class IIO : public QObject {
    Q_OBJECT

public:
    /**
     * @author      Michail Sazontev
     * @brief       Constructor
     * @param[in]   parent  parent QObject
     */
    explicit IIO(QObject *parent = nullptr) : QObject(parent) {};
    virtual ~IIO() {};

    /**
     * @author      Michail Sazontev
     * @brief       Open connection
     *
     * @param[in]   setting      The input/output connector settings
     *
     * @return      true if success
     */
    virtual bool open() noexcept = 0;
    /**
     * @author      Michail Sazontev
     * @brief       Close interface
     */
    virtual void close(void) noexcept = 0;

    /**
     * @author      Michail Sazontev
     * @brief       Return interface status
     *
     * @return      Status, true if open
     */
    virtual bool isOpen() noexcept = 0;

    /**
     * @author      Michail Sazontev
     * @brief       Write data to the interface
     *
     * @param[in]   data        Data required to send
     *
     * @return      Status, true if send success
     */
    virtual bool writeData(const QByteArray &data) noexcept = 0;

    /**
     * @author      Michail Sazontev
     * @brief       Read data to the interface
     *
     * @param[out]   data       Еhe buffer into which the data is read
     *
     * @return      Status, true if read success
     */
    virtual bool readData(QByteArray &data) noexcept = 0;

signals:
    void DataReceive(const QByteArray &data);
    void DataTransmit(const QByteArray &data);
};

}
#endif // IIO_H
