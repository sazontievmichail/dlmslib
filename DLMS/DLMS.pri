INCLUDEPATH = $$PWD/DLMS

HEADERS += \
    $$PWD/headers/Core.h \
    $$PWD/headers/Enums.h \
    $$PWD/headers/Error.h \
    $$PWD/headers/IService.h \
    $$PWD/headers/Obis.h \
    $$PWD/headers/SelectiveAccess.h \
    $$PWD/headers/Setting.h \
    $$PWD/headers/Variant.h \
    $$PWD/sources/CryptoAES.h \
    $$PWD/sources/Services/AARQ.h \
    $$PWD/sources/Services/Action.h \
    $$PWD/sources/Services/GET.h \
    $$PWD/sources/Services/SET.h

SOURCES += \
    $$PWD/sources/Core.cpp \
    $$PWD/sources/CryptoAES.cpp \
    $$PWD/sources/IService.cpp \
    $$PWD/sources/Obis.cpp \
    $$PWD/sources/SelectiveAccess.cpp \
    $$PWD/sources/Services/AARQ.cpp \
    $$PWD/sources/Services/Action.cpp \
    $$PWD/sources/Services/GET.cpp \
    $$PWD/sources/Services/SET.cpp \
    $$PWD/sources/Variant.cpp
