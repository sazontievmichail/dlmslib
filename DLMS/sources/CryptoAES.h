#ifndef CRYPTOAES_H
#define CRYPTOAES_H

#include <QCoreApplication>

namespace DLMS {

// @TODO: в дальнейшем следует заменить на полноценную криптографическую библиотеку
// Класс явлетс оберткой над криптографическими методами используемыми в ВПО. В будующем необходимо заменить на библиотеку
class Crypto {
private:
    static unsigned char getSBoxValue(unsigned char num);
    static unsigned char getSBoxInvert(unsigned char num);
    static void expansionKey(void);
    static void addRoundKey(unsigned char round);
    static void subBytes(void);
    static void shiftRows(void);
    static unsigned char xtime(unsigned char x);
    static void mixColumns(void);

    static void invMixColumns(void);
    static void invSubBytes(void);
    static void invShiftRows(void);
    static void cipher(void);
    static void invCipher(void);
public:

    static void AES_encryptECB(const unsigned char *input, const unsigned char *encryptKey, unsigned char* output);
    static void AES_decryptECB(const unsigned char *input, const unsigned char *decryptKey, unsigned char *output);
};

}

#endif //CRYPTOAES_H
