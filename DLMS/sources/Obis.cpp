#include "DLMS/headers/Obis.h"
#include <QStringList>

namespace DLMS {
namespace  {
const int obisSize = 6;
const int obisA = 0;
const int obisB = 1;
const int obisC = 2;
const int obisD = 3;
const int obisE = 4;
const int obisF = 5;
}

Obis::Obis() {
    memset(value, 0, sizeof(value));
}

Obis::Obis(quint8(&val)[obisSize]) {
    memcpy_s(this->value, sizeof(this->value), val, sizeof(val));
}

Obis::Obis(const QByteArray &value) {
    unsigned int i = 0;
    for (auto it = value.begin(); it < value.end() && i < sizeof(this->value); it++, i++) {
        this->value[i] = *it;
    }
}

Obis::Obis(std::initializer_list<quint8> val) {
    unsigned int i = 0;
    for (auto it = val.begin(); it < val.end() && i < sizeof(this->value); it++, i++) {
        this->value[i] = *it;
    }
}

Obis::Obis(QString val) {
    val.replace(" ", "");
    QStringList list = val.split('.');
    if (sizeof(value) != list.size()) {
        memset(value, 0, sizeof(value));
    } else {
        int i = 0;
        for (auto &str : list) {
            this->value[i] = (quint8)str.toInt();
            i++;
        }
    }
}

Obis::Obis(const Obis& val) {
    memcpy_s(this->value, sizeof(this->value), val.value, sizeof (val.value));
}

Obis& Obis::operator=(const Obis &val) {
    memcpy_s(this->value, sizeof(this->value), val.value, sizeof (val.value));
    return *this;
}

Obis& Obis::operator=(std::initializer_list<quint8> val) {
    unsigned int i = 0;
    for (auto it = val.begin(); it < val.end() && i < sizeof(value); it++, i++) {
        this->value[i] = *it;
    }
    return *this;
}

Obis& Obis::operator=(const QByteArray &val) {
    for (int i = 0; i < (int)sizeof(value) && i < val.size(); i++) {
        this->value[i] = val[i];
    }
    return *this;
}

bool operator>(const Obis &left, const Obis &right) {
    return (left.toNumber() > right.toNumber());
}

bool operator<(const Obis &left, const Obis &right) {
    return (left.toNumber() < right.toNumber());
}

bool operator==(const Obis &left, const std::initializer_list<qint16> &right) {
    unsigned int i = 0;
    for (auto it = right.begin(); it < right.end() && i < sizeof (left.value); it++, i++) {
        if (*it < 0) {
            continue;
        }
        if (*it != left.value[i]) {
            return false;
        }
    }
    return true;
}

quint8 Obis::getA() {
    return this->value[obisA];
}

quint8 Obis::getB() {
    return this->value[obisB];
}

quint8 Obis::getC() {
    return this->value[obisC];
}

quint8 Obis::getD() {
    return this->value[obisD];
}

quint8 Obis::getE() {
    return this->value[obisE];
}

quint8 Obis::getF() {
    return this->value[obisF];
}

QByteArray Obis::Data() const {
    return QByteArray::fromRawData((char*)value, obisSize);
}

quint64 Obis::toNumber() const {
    qint64 val = 0;
    memcpy_s(&val, sizeof(val), value, sizeof(value));
    return val;
}

QString Obis::toString() const {
    QString val = "";
    for (int i = 0; i < (int)sizeof(value); i++) {
        val += QString::number((int)value[i]);
        if (i < (int)sizeof(value) - 1) {
            val += ".";
        }
    }
    return val;
}

bool Obis::isNull() const {
    return toNumber() == 0;
}

}
