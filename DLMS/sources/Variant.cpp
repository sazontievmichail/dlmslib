#include "DLMS/headers/Variant.h"
#include "HDLC/sources/Utils.h"

namespace DLMS
{
namespace  {
const int dataTimeSize = 12;
const int dataSize = 5;
const int obisSize = 6;
const quint8 dataLenTag8 = 0x81;
const quint8 dataLenTag16 = 0x82;
const Obis nullObis = {0, 0, 0, 0, 0, 0};
}

Variant::Variant() {
    data = QByteArray();
    type = DataType::NONE;

    boolVal = true;
    floatVal = 0.;
    doubleVal = 0.;
    intVal = 0;
    uintVal = 0;
}

Variant::Variant(bool value) {
    type = DataType::BOOLEAN;
    boolVal = value;
}

Variant::Variant(float value) {
    type = DataType::FLOAT32;
    floatVal = value;
}

Variant::Variant(double value) {
    type = DataType::FLOAT64;
    doubleVal = value;
}

Variant::Variant(quint8 value) {
    type = DataType::UINT8;
    uintVal = value;
}

Variant::Variant(quint16 value) {
    type = DataType::UINT16;
    uintVal = value;
}

Variant::Variant(quint32 value) {
    type = DataType::UINT32;
    uintVal = value;
}

Variant::Variant(quint64 value) {
    type = DataType::UINT64;
    uintVal = value;
}
Variant::Variant(qint8 value) {
    type = DataType::INT8;
    intVal = value;
}

Variant::Variant(qint16 value) {
    type = DataType::INT16;
    intVal = value;
}

Variant::Variant(qint32 value) {
    type = DataType::INT32;
    intVal = value;
}

Variant::Variant(qint64 value) {
    type = DataType::INT64;
    intVal = value;
}

Variant::Variant(const QByteArray &value) {
    type = DataType::OCTET_STRING;
    data.append(value);
}

Variant::Variant(const QVector<Variant> &value, bool isStruct) {
    type = (isStruct) ? DataType::STRUCTURE : DataType::ARRAY;
    listVal = value;
}

Variant::Variant(const QDateTime dateTime, quint16 deviation, quint8 status) {
    type = DataType::OCTET_STRING;

    data << (quint16)dateTime.date().year();
    data << (quint8)dateTime.date().month();
    data << (quint8)dateTime.date().day();
    data << (quint8)dateTime.date().dayOfWeek();

    data << (quint8)dateTime.time().hour();
    data << (quint8)dateTime.time().minute();
    data << (quint8)dateTime.time().second();
    data << (quint8)dateTime.time().msec();

    data << (quint16)deviation;
    data << (quint8)status;
}

Variant::Variant(const QDate date) {
    type = DataType::OCTET_STRING;
    data << (quint16)date.year();
    data << (quint8)date.month();
    data << (quint8)date.day();
    data << (quint8)date.dayOfWeek();
}

Variant::~Variant() {

};

bool Variant::isNull() const {
    return (DataType::NONE == type);
}

bool Variant::isArray() const {
    return (DataType::ARRAY == type);
}

bool Variant::isStruct() const {
    return (DataType::STRUCTURE == type);
}

bool Variant::isNumber() const {
    switch ((quint8)type) {
        case (quint8)DataType::UINT8:
        case (quint8)DataType::UINT16:
        case (quint8)DataType::UINT32:
        case (quint8)DataType::UINT64:
        case (quint8)DataType::INT8:
        case (quint8)DataType::INT16:
        case (quint8)DataType::INT32:
        case (quint8)DataType::INT64:
        case (quint8)DataType::FLOAT32:
        case (quint8)DataType::FLOAT64:
            return true;
        default:
            return false;
    }
}

bool Variant::isBoolean() const {
    return (DataType::BOOLEAN == type);
}

bool Variant::isOctetString() const {
    return (DataType::OCTET_STRING == type);
}

bool Variant::isObis() const {
    return (DataType::OCTET_STRING == type && obisSize == data.size());
}

bool Variant::isDateTime() const {
    return dataTimeSize == data.size();
}

bool Variant::isDate() const {
    return dataSize == data.size();
}

bool Variant::isEnum() const {
    return DataType::ENUM == type;
}

qint64 Variant::toNumber() const {
    //@TODO: Необходимо реализация для типа BIT_STRING
    //@TODO: Необходимо реализация для типа STRING
    //@TODO: Необходимо реализация для типа STRING_UTF8
    //@TODO: Необходимо реализация для типа BINARY_CODED_DESIMAL
    //@TODO: Необходимо реализация для типа COMPACT_ARRAY
    //@TODO: Необходимо реализация для типа DATETIME
    //@TODO: Необходимо реализация для типа DATE
    //@TODO: Необходимо реализация для типа TIME
    switch((quint8)type) {
        case (quint8)DataType::INT8:
        case (quint8)DataType::INT16:
        case (quint8)DataType::INT32:
        case (quint8)DataType::INT64:
            return (qint64)intVal;
            break;
        case (quint8)DataType::ENUM:
        case (quint8)DataType::UINT8:
        case (quint8)DataType::UINT16:
        case (quint8)DataType::UINT32:
        case (quint8)DataType::UINT64:
            return (qint64)uintVal;
            break;
        default:
            return 0;
    }
}

QString Variant::toString() const {
    QString res = "";
    //@TODO: Необходимо реализация для типа BIT_STRING
    //@TODO: Необходимо реализация для типа STRING_UTF8
    //@TODO: Необходимо реализация для типа BINARY_CODED_DESIMAL
    //@TODO: Необходимо реализация для типа COMPACT_ARRAY
    //@TODO: Необходимо реализация для типа DATETIME
    //@TODO: Необходимо реализация для типа DATE
    //@TODO: Необходимо реализация для типа TIME
    switch ((quint8)type) {
        case (quint8)DataType::NONE:
            return "NONE";
        case (quint8)DataType::BOOLEAN:
            return boolVal ? "True" : "False";
        case (quint8)DataType::INT8:
        case (quint8)DataType::INT16:
        case (quint8)DataType::INT32:
        case (quint8)DataType::INT64:
            return QString::number(intVal);
        case (quint8)DataType::ENUM:
        case (quint8)DataType::UINT8:
        case (quint8)DataType::UINT16:
        case (quint8)DataType::UINT32:
        case (quint8)DataType::UINT64:
            return QString::number(uintVal);
        case (quint8)DataType::OCTET_STRING:
            return QString(data.toHex().toUpper());
        case (quint8)DataType::STRING:
            return QString(data.data());
        case (quint8)DataType::STRUCTURE:   //@TODO: используется для отладки, в дальнейшем стоит полностью убрать
        case (quint8)DataType::ARRAY:
            res += "size: " + QString::number(listVal.size());
            res += "\n{";
            for (auto val : listVal) {
                res += "\n\t" + val.toString();
            }
            res += "\n}";
            return res;
        default:
            return "Underfined";
    }
}

bool Variant::toBoolean() const {
    if (!isBoolean()) {
        return false;
    }
    return boolVal;
}

QByteArray Variant::toOctetString() const {
    //@TODO: Необходимо реализация для типа BIT_STRING
    //@TODO: Необходимо реализация для типа STRING
    //@TODO: Необходимо реализация для типа STRING_UTF8
    //@TODO: Необходимо реализация для типа BINARY_CODED_DESIMAL
    //@TODO: Необходимо реализация для типа COMPACT_ARRAY
    //@TODO: Необходимо реализация для типа DATETIME
    //@TODO: Необходимо реализация для типа DATE
    //@TODO: Необходимо реализация для типа TIME
    switch ((quint8)type) {
        case (quint8)DataType::OCTET_STRING:
            return data;
        default:
            return QByteArray();
    }
}

Obis Variant::toObis() const {
    return isObis() ? this->data : nullObis;
}

QString Variant::toXML() const {
    switch ((quint8)type) {
        case (quint8)DataType::NONE:
            return "<NONE></NONE>";
        case (quint8)DataType::BOOLEAN:
            return "<BOOL>" + QString(boolVal ? "True" : "False") + "</BOOL>";
        case (quint8)DataType::INT8:
            return "<INT8>" + QString::number(intVal) + "</INT8>";
        case (quint8)DataType::INT16:
            return "<INT16>" + QString::number(intVal) + "</INT16>";
        case (quint8)DataType::INT32:
            return "<INT32>" + QString::number(intVal) + "</INT32>";
        case (quint8)DataType::INT64:
            return "<INT64>" + QString::number(intVal) + "</INT64>";
        case (quint8)DataType::ENUM:
            return "<ENUM>" + QString::number(uintVal) + "</ENUM>";
        case (quint8)DataType::UINT8:
            return "<UINT8>" + QString::number(uintVal) + "</UINT8>";
        case (quint8)DataType::UINT16:
            return "<UINT16>" + QString::number(uintVal) + "</UINT16>";
        case (quint8)DataType::UINT32:
            return "<UINT32>" + QString::number(uintVal) + "</UINT32>";
        case (quint8)DataType::UINT64:
            return "<UINT64>" + QString::number(uintVal) + "</INT8>";
        case (quint8)DataType::OCTET_STRING:
            return "<OCTET_STRING>" + QString(data.toHex().toUpper()) + "</OCTET_STRING>";
        case (quint8)DataType::STRUCTURE:
        case (quint8)DataType::ARRAY:
        {
            QString res = (type == DataType::ARRAY) ? "<ARRAY " : "<STRUCTURE ";
            res += "count='" + QString::number(listVal.size()) + "'>";
            for (auto &val : listVal) {
                res += val.toXML();
            }
            res += (type == DataType::ARRAY) ? "</ARRAY>" : "</STRUCTURE>";
            return res;
        }
        default:
            return "<UNDERFINED></UNDERFINED>";
    }
}

QDateTime Variant::toDateTime() const {
    QDateTime result(QDate(1994, 7, 24), QTime(23, 45));    // Мой день рождения, ничего магического, просто Я.
    if (!isDateTime()) {
        return result;
    }
    auto it = data.begin();
    int y = ReadUINT16(it, data);
    int m = ReadUINT8(it, data);
    int d = ReadUINT8(it, data);
    result.setDate(QDate(y, m, d));
    ReadUINT8(it, data);    // Day of weak
    int h = ReadUINT8(it, data);
    m = ReadUINT8(it, data);
    int s  = ReadUINT8(it, data);
    int ms = ReadUINT8(it, data);
    result.setTime(QTime(h, m, s, ms));
    return result;
}

QDate Variant::toDate() const {
    QDate result(1994, 7, 24);  // Мой день рождения, ничего магического, просто Я.
    if (!isDate()) {
        return result;
    }
    auto it = data.begin();
    int y = ReadUINT16(it, data);
    int m = ReadUINT8(it, data);
    int d = ReadUINT8(it, data);
    result.setDate(y, m, d);
    ReadUINT8(it, data);    // Day of weak
    return result;
}

const QVector<Variant> &Variant::toArray() const {
    return listVal;
}

Variant& Variant::operator= (bool right) {
    clear();
    type = DataType::BOOLEAN;
    boolVal = right;
    return *this;
}

Variant& Variant::operator= (quint8 right) {
    clear();
    type = DataType::UINT8;
    uintVal = right;
    return *this;
}

Variant& Variant::operator= (quint16 right) {
    clear();
    type = DataType::UINT16;
    uintVal = right;
    return *this;
}

Variant& Variant::operator= (quint32 right) {
    clear();
    type = DataType::UINT32;
    uintVal = right;
    return *this;
}

Variant& Variant::operator= (quint64 right) {
    clear();
    type = DataType::UINT64;
    uintVal = right;
    return *this;
}

Variant& Variant::operator= (qint8 right) {
    clear();
    type = DataType::INT8;
    intVal = right;
    return *this;
}

Variant& Variant::operator= (qint16 right) {
    clear();
    type = DataType::INT16;
    intVal = right;
    return *this;
}

Variant& Variant::operator= (qint32 right) {
    clear();
    type = DataType::INT32;
    intVal = right;
    return *this;
}

Variant& Variant::operator= (qint64 right) {
    clear();
    type = DataType::INT64;
    intVal = right;
    return *this;
}

Variant& Variant::operator= (const QByteArray& right) {
    clear();
    type = DataType::OCTET_STRING;
    data = right;
    return *this;
}

Variant& Variant::operator= (const QVector<Variant>& right) {
    clear();
    type = DataType::ARRAY;
    listVal = right;
    return *this;
}

const Variant& Variant::operator[] (quint32 val) const {
    if(DataType::ARRAY != type && DataType::STRUCTURE != type) {
        throw Error::InvalidType;
    }
    if ((quint32)this->listVal.size() < val) {
        throw Error::OutOfMemory;
    }
    return listVal[val];
}

int Variant::size() const {
    switch ((quint8)type) {
        case (quint8) DataType::ARRAY:
        case (quint8) DataType::STRUCTURE:
            return listVal.size();
        default:
            return 0;
    }
}

void Variant::clear() {
    listVal.clear();
    intVal = 0;
    uintVal = 0;
    floatVal = 0;
    doubleVal = 0;
    type = DataType::NONE;
}

const QByteArray& Variant::getData() const {
    return data;
}

QByteArray Variant::xdrData() const {
    QByteArray res;
    res << (quint8)type;

    //@TODO: Необходимо реализация для типа BIT_STRING
    //@TODO: Необходимо реализация для типа STRING
    //@TODO: Необходимо реализация для типа STRING_UTF8
    //@TODO: Необходимо реализация для типа BINARY_CODED_DESIMAL
    //@TODO: Необходимо реализация для типа COMPACT_ARRAY
    //@TODO: Необходимо реализация для типа DATETIME
    //@TODO: Необходимо реализация для типа DATE
    //@TODO: Необходимо реализация для типа TIME
    switch ((quint8)type) {
        case (quint8)DataType::NONE:
            res << (quint8)0x00;
            break;
        case (quint8)DataType::BOOLEAN:
            res << (quint8)boolVal;
            break;
        case (quint8)DataType::OCTET_STRING:
            res << (quint8)data.size();
            res << data;
            break;
        case (quint8)DataType::UINT8:
        case (quint8)DataType::ENUM:
            res << (quint8)uintVal;
            break;
        case (quint8)DataType::UINT16:
            res << (quint16)uintVal;
            break;
        case (quint8)DataType::UINT32:
            res << (quint32)uintVal;
            break;
        case (quint8)DataType::UINT64:
            res << (quint64)uintVal;
            break;
        case (quint8)DataType::INT8:
            res << (quint8)intVal;
            break;
        case (quint8)DataType::INT16:
            res << (quint16)intVal;
            break;
        case (quint8)DataType::INT32:
            res << (quint32)intVal;
            break;
        case (quint8)DataType::INT64:
            res << (quint64)intVal;
            break;
        case (quint8)DataType::STRUCTURE:
        case (quint8)DataType::ARRAY:
            if (128 > listVal.size()) {         // 128 - пороговое значение размера
                res << (quint8)listVal.size();
            } else if (255 < listVal.size()) {  // 255 - максимальное значение unsigned char
                res << (quint8)dataLenTag16;
                res << (quint16)listVal.size();
            } else {
                res << (quint8)dataLenTag8;
                res << (quint8)listVal.size();
            }
            for (auto &val : listVal) {
                res << val.xdrData();
            }
            break;
        default:
            throw Error::UnsupportedType;
    }
    return res;
}

Variant Variant::parseValue(const QByteArray &data) {
    QByteArray::const_iterator it = data.cbegin();

    return parse(it, data);
}

void Variant::checkTagLen(QByteArray::const_iterator &it, const QByteArray &data) {
    if (it >= data.cend() || *it > data.cend() - it) {
        throw Error::InvalidSize;
    }
}

Variant Variant::parse(QByteArray::const_iterator &it, const QByteArray &data) {
    if (it == data.cend()) {
        throw Error::InvalidSize;
    }

    Variant value;
    value.type = static_cast<DataType>(*it);

    quint16 len = 0;
    quint8 type = ReadUINT8(it, data);

    //@TODO: Необходимо реализация для типа BIT_STRING
    //@TODO: Необходимо реализация для типа STRING_UTF8
    //@TODO: Необходимо реализация для типа BINARY_CODED_DESIMAL
    //@TODO: Необходимо реализация для типа COMPACT_ARRAY
    //@TODO: Необходимо реализация для типа DATETIME
    //@TODO: Необходимо реализация для типа DATE
    //@TODO: Необходимо реализация для типа TIME
    switch (type) {
        case (quint8)DataType::NONE:
            break;
        case (quint8)DataType::BOOLEAN:
            value.boolVal = (bool)ReadUINT8(it,data);
            break;
        case (quint8)DataType::OCTET_STRING:
        case (quint8)DataType::STRING:
            len = *it;
            checkTagLen(it, data);
            it++;
            value.data = data.mid(it - data.begin(), len);
            it += len;
            break;
        case (quint8)DataType::UINT8:
        case (quint8)DataType::ENUM:
            value.uintVal = ReadUINT8(it, data);
            break;
        case (quint8)DataType::UINT16:
            value.uintVal = ReadUINT16(it, data);
            break;
        case (quint8)DataType::UINT32:
            value.uintVal = ReadUINT32(it, data);
            break;
        case (quint8)DataType::UINT64:
            value.uintVal = ReadUINT64(it,data);
            break;
        case (quint8)DataType::INT8:
            value.intVal = (qint8)ReadUINT8(it, data);
            break;
        case (quint8)DataType::INT16:
            value.intVal = (qint16)ReadUINT16(it, data);
            break;
        case (quint8)DataType::INT32:
            value.intVal = (qint32)ReadUINT32(it, data);
            break;
        case (quint8)DataType::INT64:
            value.intVal = (qint64)ReadUINT64(it,data);
            break;
        case (quint8)DataType::STRUCTURE:
        case (quint8)DataType::ARRAY:
            len = getTagLen(it, data);
            for (int i = 0; i < len; i++) {
                value.listVal.push_back(parse(it, data));
            }
            break;
        default:
            throw Error::UnsupportedType;
    }
    return value;
}

quint16 Variant::getTagLen(QByteArray::const_iterator &it, const QByteArray &data) {
    quint8 len = ReadUINT8(it, data);
    if (dataLenTag8 == len) {
        return ReadUINT8(it, data);
    } else if (dataLenTag16 == len) {
        return ReadUINT16(it, data);
    } else {
        return len;
    }
}

}

