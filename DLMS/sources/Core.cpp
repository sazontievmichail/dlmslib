#include "DLMS/headers/Core.h"
#include "Services/AARQ.h"
#include "Services/Action.h"
#include "Services/SET.h"
#include "Services/GET.h"
#include "CryptoAES.h"

namespace DLMS {
namespace  {
const Obis currentAssociationLN = {0,0,40,0,0,255};
const int connectionAttribute = 1;
const int connectionStateAttribute = 8;
const int hdlcErrorShift = 8;
const int bufferSize = 1024;
}

Core::Core() :
    Action(new SAction(*this)),
    GET(new SGET(*this)),
    SET(new SSET(*this)),
    AARQ(new SAARQ(*this)) {
    status = ConnectionStatus::NO;
};

ConnectionStatus Core::open(const Setting &dlmsSetting,
                            const HDLC::IOSetting &ioSetting,
                            quint8 clientAddr, quint16 srvLogicalAddr, quint16 srvPhysicalAddr) {
    this->initDlmsSettings = dlmsSetting;
    this->usingDlmsSettings = dlmsSetting;
    this->clientAddr = clientAddr;
    this->srvLogicalAddr = srvLogicalAddr;
    this->srvPhysicalAddr = srvPhysicalAddr;

    HDLC::Error hdlcErr = hdlcClient.open(ioSetting, clientAddr, srvLogicalAddr, srvPhysicalAddr);

    if (HDLC::Error::Ok != hdlcErr) {
        throw static_cast<Error>((quint8)hdlcErr << hdlcErrorShift);
    }
    status = ConnectionStatus::HDLC_ESTABLISHED;

    AARQ->querry(this->initDlmsSettings, this->usingDlmsSettings);
    if (ConnectionStatus::DLMS_PENDING == status) {
        unsigned char buff[bufferSize] = {0};
        Crypto::AES_encryptECB((const unsigned char*)usingDlmsSettings.sToC.constData(), (const unsigned char*)usingDlmsSettings.authenticationKey.toStdString().c_str(), buff);
        Variant sToC = QByteArray::fromRawData((const char*)buff, usingDlmsSettings.sToC.size());
        Variant cToS;
        Action->query(currentAssociationLN, connectionAttribute, ClassType::ASSOCIATION_LN, sToC, cToS);

        Crypto::AES_decryptECB((const unsigned char*)cToS.getData().constData(), (const unsigned char*)usingDlmsSettings.authenticationKey.toStdString().c_str(), buff);
        if (QByteArray::fromRawData((const char*)buff, usingDlmsSettings.sToC.size()) == usingDlmsSettings.cToS) {
            status = ConnectionStatus::DLMS_ESTABLISHED;
        }
    }
    return status;
}

void Core::close() {
    if (ConnectionStatus::NO < status) {
        hdlcClient.close();
        status = ConnectionStatus::NO;
    }
}

void Core::transfer(const QByteArray &txData, QByteArray &rxData) {
    if (ConnectionStatus::HDLC_ESTABLISHED > status) {
        throw Error::HDLCConnectionNotOpen;
    }

    QByteArray passData = QByteArray::fromHex(QString("E6 E6 00").toLatin1()) + txData; // 0xE6 0xE6 0x00 - dlms header and direction to server
    HDLC::Error err = hdlcClient.transfer(passData, rxData);
    if (HDLC::Error::Ok != err) {
        throw static_cast<Error>((quint8)err << 8);
    }
    if (3 > rxData.size()) {
        throw Error::InvalidSize;
    }
    if ((char)0xE6 != rxData[0] || (char)0xE7 != rxData[1] || (char)0x00 != rxData[2]) {    // 0xE6 0xE7 0x00 - dlms header and direction to client
        throw Error::InvalidDirection;
    }
    if ((char)Command::EXCEPTION_RESPONSE == rxData[3]) {
        switch (rxData[4]) {
        case 0x01:          // connection state, 0x01 - connection not open
            throw Error::DLMSConnectionNotOpen;
        default:
            throw Error::ExceptionUnknown;
        }
    }
    rxData = rxData.right(rxData.size() - 3);       // cut dlms header, 3 - size of dlms header
}

ConnectionStatus Core::getConnectionStatus() noexcept {
    if (ConnectionStatus::NO == status) {
        return status;
    }
    try {
        Variant res;
        GET->query(currentAssociationLN, connectionStateAttribute, ClassType::ASSOCIATION_LN, Variant(), res);

        switch (res.toNumber()) {
            case (quint8)MLRDLMSConnectionStatus::CONNECTION_STATUS_NO:
                status = ConnectionStatus::NO;
                break;
            case (quint8)MLRDLMSConnectionStatus::CONNECTION_STATUS_PENDING:
                status = ConnectionStatus::DLMS_PENDING;
                break;
            case (quint8)MLRDLMSConnectionStatus::CONNECTION_STATUS_ESTABLISHED:
                status = ConnectionStatus::DLMS_ESTABLISHED;
                break;
            default:
                status = ConnectionStatus::HDLC_ESTABLISHED;
                break;
        }
    }  catch (HDLC::Error) {
        status = ConnectionStatus::NO;
    }  catch (DLMS::Error) {
        status = ConnectionStatus::HDLC_ESTABLISHED;
    }
    return status;
}

HDLC::Core& Core::getHDLC() {
    return hdlcClient;
}

Core::~Core() {
    if (nullptr != AARQ) {
        delete AARQ;
        AARQ = nullptr;
    }
    if (nullptr != Action) {
        delete Action;
    }
    if (nullptr != GET) {
        delete GET;
    }
    if (nullptr != SET) {
        delete SET;
    }

    close();
}

}
