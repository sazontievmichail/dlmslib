#include "DLMS/headers/IService.h"

namespace DLMS {

IService::IService(Core &core) {
    this->core = &core;
}

IService::IService(IService &service) {
    this->core = service.core;
}

IService::~IService() {
    this->core = nullptr;
}

}
