#include "Action.h"
#include "HDLC/sources/Utils.h"
#include "DLMS/headers/Core.h"

namespace DLMS {

SAction::SAction(Core &core) : IService(core) {

};

QByteArray SAction::handleRequest(const Obis &dlmsObj, const quint8 method, const ClassType classType, const Variant &data, SelectiveAccess::Type) {
    QByteArray result;
    result.clear();
    result << (quint8)Command::METHOD_REQUEST;
    result << (quint8)DLMSActionRequest::NORMAL;
    result << (quint8)0xC1;
    result << (quint8)0x00;
    result << (quint8)classType;
    result << dlmsObj.Data();
    result << (quint8)method;

    if (data.isNull()) {
        result << (quint8)0x00;           // 0x00 - data is null
    } else {
        result << (quint8)0x01;           // 0x01 - data is contains
        result << data.xdrData();
    }
    return result;
}

quint8 SAction::handleResponse(const QByteArray &data, Variant &result) {
    QByteArray::const_iterator it = data.begin();

    if ((quint8)Command::METHOD_RESPONSE != ReadUINT8(it, data)) {
        throw Error::InvalidMethod;
    }

    //@TODO: Не реализованно получение данных типа NEXT_PBLOCK
    //@TODO: Не реализованно получение данных типа WITH_LIST
    switch (ReadUINT8(it, data)) {
        case (quint8)DLMSActionRequest::NORMAL:
            break;
        case (quint8)DLMSActionRequest::NEXT_PBLOCK:
        case (quint8)DLMSActionRequest::WITH_PBLOCK:
        case (quint8)DLMSActionRequest::WITH_LIST:
        case (quint8)DLMSActionRequest::WITH_FIRST_PBLOCK:
        case (quint8)DLMSActionRequest::WITH_LIST_AND_FIRST_PBLOCK:
            throw Error::UnsupportedMethod;
        default:
            throw Error::InvalidMethod;
            break;
    }

    ReadUINT8(it, data); // C1

    quint8 res =  ReadUINT8(it, data);
    if (res != 0x00) {      // 0x00 - ok, else error code
        return res;
    }

    if (0x01 == ReadUINT8(it, data)) {      // 0x01 - data is contains
        result = Variant::parseValue(data.right(data.end() - (it + 1)));
    }
    return res;
}

quint8 SAction::query(const Obis &dlmsObj, const quint8 method, const ClassType classType, const Variant &data, Variant &result, SelectiveAccess::Type) {
    if (ConnectionStatus::DLMS_PENDING > core->status) {
        throw Error::DLMSConnectionNotOpen;
    }
    quint8 err = 0;
    QByteArray tmp;
    QByteArray res;

    tmp = handleRequest(dlmsObj, method, classType, data);
    this->core->transfer(tmp, res);
    err = handleResponse(res, result);

    return err;
}

}
