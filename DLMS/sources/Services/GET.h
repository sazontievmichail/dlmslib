#ifndef GET_H
#define GET_H

#include "DLMS/headers/Obis.h"
#include "DLMS/headers/Enums.h"
#include "DLMS/headers/Variant.h"
#include "DLMS/headers/IService.h"
#include "DLMS/headers/SelectiveAccess.h"

namespace DLMS {

enum class DLMSGetRequest : quint8 {
    NORMAL      = 1,
    NEXT        = 2,
    WITH_LIST   = 3
} ;

enum class DLMSGetResponse : quint8 {
    NORMAL              = 1,
    WITH_DATABLOCK      = 2,
    WITH_LIST           = 3,
};

class SGET : public IService {
public:
    SGET(Core &core) : IService(core) {};

    quint8 query(const Obis &dlmsObj
                 , const quint8 method
                 , const ClassType classType
                 , const Variant &data
                 , Variant &result
                 , SelectiveAccess::Type selectiveAccsess = SelectiveAccess::Type::NONE);

protected:
    QByteArray handleRequest(const Obis &dlmsObj, const quint8 attribute, const ClassType classType, const Variant &data, SelectiveAccess::Type selectiveAccess);
    quint8 handleResponse(const QByteArray &data, Variant &result);

private:
    QByteArray requestGetNextDataBlock(int packageNum);
    quint8 handleResponceNormal(QByteArray::const_iterator &it, const QByteArray &data, Variant &result);
    quint8 handleResponceWithBlock(const QByteArray &data, Variant &result);
};

}


#endif // GET_H
