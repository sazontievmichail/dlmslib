#ifndef AARQ_H
#define AARQ_H

#include "DLMS/headers/Enums.h"
#include "DLMS/headers/Setting.h"

namespace DLMS {

class Core;

class SAARQ {
    enum BERTypes {
        EOC                 = 0x00,     // End of Content.
        BOOLEAN             = 0x1,      // Boolean
        INTEGER             = 0x2,      // Integer
        BIT_STRING          = 0x3,      // Bit String
        OCTET_STRING        = 0x4,      // Octet string
        NULL_VALUE          = 0x5,      // Null value

        OBJECT_IDENTIFIER   = 0x6,      // Object identifier
        OBJECT_DESCRIPTOR   = 7,        // Object Descriptor
        EXTERNAL            = 8,        // External
        REAL                = 9,        // Real (float)
        ENUMERATED          = 10,       // Enumerated.
        UTF8_STRING         = 12,       // Utf8 String.

        NUMERIC_STRING      = 18,       // Numeric string
        PRINTABLE_STRING    = 19,       // Printable string
        TELETEX_STRING      = 20,       // Teletex string
        VIDEOTEX_STRING     = 21,       // Videotex string
        IA5_STRING          = 22,       // Ia5 string
        UTC_TIME            = 23,       // Utc time
        GENERALIZED_TIME    = 24,       // Generalized time
        GRAPHIC_STRING      = 25,       // Graphic string
        VISIBLE_STRING      = 26,       // Visible string
        GENERAL_STRING      = 27,       // General string
        UNIVERSAL_STRING    = 28,       // Universal string
        BMP_STRING          = 30,       // Bmp string

        APPLICATION         = 0x40,     // Application class
        CONTEXT             = 0x80,     // Context class
        PRIVATE             = 0xc0,     // Private class
        CONSTRUCTED         = 0x20      // Constructed
    };
    enum APDUType {
        PROTOCOL_VERSION                = 0,    // IMPLICIT BIT STRING {version1  = 0} DEFAULT {version1}
        APPLICATION_CONTEXT_NAME        = 1,    // Application-context-name
        CALLED_AP_TITLE                 = 2,    // AP-title OPTIONAL
        CALLED_AE_QUALIFIER             = 3,    // AE-qualifier OPTIONAL.
        CALLED_AP_INVOCATION_ID         = 4,    // AP-invocation-identifier OPTIONAL.
        CALLED_AE_INVOCATION_ID         = 5,    // AE-invocation-identifier OPTIONAL
        CALLING_AP_TITLE                = 6,    // AP-title OPTIONAL
        CALLING_AE_QUALIFIER            = 7,    // AE-qualifier OPTIONAL
        CALLING_AP_INVOCATION_ID        = 8,    // AP-invocation-identifier OPTIONAL
        CALLING_AE_INVOCATION_ID        = 9,    // AE-invocation-identifier OPTIONAL
        SENDER_ACSE_REQUIREMENTS        = 10,   // The following field shall not be present if only the kernel is used.
        MECHANISM_NAME                  = 11,   // The following field shall only be present if the authentication functional unit is selected.
        CALLING_AUTHENTICATION_VALUE    = 12,   // The following field shall only be present if the authentication functional unit is selected.
        IMPLEMENTATION_INFORMATION      = 29,   // Implementation-data.
        USER_INFORMATION                = 30    // Association-information OPTIONAL
    };
public:
    SAARQ(Core &core);

    quint8 querry(const Setting &setting, Setting &serverSetting);

protected:
    void handleRequest(const Setting &setting, Setting &severSetting, QByteArray &data);
    void handleResponse(Setting &setting, QByteArray &data);

private:
    void getObjectCount(QByteArray::const_iterator &it, const QByteArray &data, quint32 &count);

    void skipTag(QByteArray::const_iterator &it, const QByteArray &data);
    void checkTagLen(QByteArray::const_iterator &it, const QByteArray &data);

    QByteArray generateCtoSChallenge(quint16 len);
    void generateApplicationContextName(const Setting &setting, QByteArray &data);
    void generateAuthenticationData(const Setting &setting, Setting &serverSetting, QByteArray &data);
    void generateUserInformation(const Setting &setting, QByteArray &data);

    void parseAplicationContextName(Setting &setting, QByteArray::const_iterator &it, const QByteArray &data);
    void parseAssociationResult(QByteArray::const_iterator &it, const QByteArray &data);
    void parseSourceDiagnostic(QByteArray::const_iterator &it, const QByteArray &data);
    void parseAuthenticationData(Setting &setting, QByteArray::const_iterator &it, const QByteArray &data);
    void parseUserInformation(Setting &setting, QByteArray::const_iterator &it, const QByteArray &data);
    void parseAuthenticationFunctional(Setting &setting, QByteArray::const_iterator &it, const QByteArray &data);

protected:
    AuthenticationResult authenticationResult;

private:
    Core *core;
};


}

#endif // AARQ_H
