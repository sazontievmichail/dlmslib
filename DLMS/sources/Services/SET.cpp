#include "SET.h"
#include "HDLC/sources/Utils.h"
#include "DLMS/headers/Core.h"

namespace DLMS
{

quint8 SSET::query(const Obis &dlmsObj, const quint8 attribute, const ClassType classType, const Variant &data, Variant &result, SelectiveAccess::Type) {
    if (ConnectionStatus::DLMS_ESTABLISHED != core->status) {
        throw Error::DLMSConnectionNotOpen;
    }
    quint8 err = 0;
    QByteArray req;
    QByteArray res;

    req = handleRequest(dlmsObj, attribute, classType, data);
    this->core->transfer(req, res);
    err = handleResponse(res, result);

    return err;
}

QByteArray SSET::handleRequest(const Obis &dlmsObj, const quint8 attribute, const ClassType classType, const Variant &data, SelectiveAccess::Type) {
    QByteArray result;
    result << (quint8)Command::SET_REQUEST;
    result << (quint8)DLMSSetRequest::NORMAL;
    result << (quint8)0xC1;       // service id
    result << (quint8)0x00;       // invoke id
    result << (quint8)classType;
    result << dlmsObj.Data();
    result << (quint8)attribute;

    result << (quint8)0x00;       // access token - ignored
    result << data.xdrData();

    return result;
}

quint8 SSET::handleResponse(const QByteArray &data, Variant &result) {
    QByteArray::const_iterator it = data.begin();
    if ((quint8)Command::SET_RESPONSE != ReadUINT8(it, data)) {
        throw Error::InvalidMethod;
    }
    switch (ReadUINT8(it, data)) {
        case (quint8)DLMSSetResponse::NORMAL:
            ReadUINT8(it, data);
            result.clear();
            return ReadUINT8(it, data);
            break;
        case (quint8)DLMSSetResponse::WITH_DATABLOCK:
        case (quint8)DLMSSetResponse::WITH_LIST:
            throw Error::UnsupportedMethod;
        default:
            throw Error::InvalidMethod;
            break;
    }
}

}
