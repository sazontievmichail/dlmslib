#ifndef ACTION_H
#define ACTION_H

#include "DLMS/headers/Obis.h"
#include "DLMS/headers/Enums.h"
#include "DLMS/headers/Variant.h"
#include "DLMS/headers/IService.h"
#include "DLMS/headers/SelectiveAccess.h"

namespace DLMS {

class Core;

enum class DLMSActionRequest : quint8 {
    NORMAL                          = 1,
    /* Поддерживаетс только NORMAL */
    NEXT_PBLOCK,
    WITH_LIST,
    WITH_FIRST_PBLOCK,
    WITH_LIST_AND_FIRST_PBLOCK,
    WITH_PBLOCK
};

class SAction : public IService {
public:
    SAction(Core &core);

    quint8 query(const Obis &dlmsObj
                 , const quint8 method
                 , const ClassType classType
                 , const Variant &data
                 , Variant &result
                 , SelectiveAccess::Type selectiveAccsess = SelectiveAccess::Type::NONE);

protected:
    QByteArray handleRequest(const Obis &dlmsObj, const quint8 method, const ClassType classType, const Variant &data,
                             SelectiveAccess::Type selectiveAccess = SelectiveAccess::Type::NONE);
    quint8 handleResponse(const QByteArray &data, Variant &result);
};

}

#endif // ACTION_H
