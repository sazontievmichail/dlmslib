#include "GET.h"
#include "HDLC/sources/Utils.h"
#include "DLMS/headers/Core.h"

namespace DLMS
{
namespace  {
const quint8 dataLenTag8 = 0x81;
const quint8 dataLenTag16 = 0x82;
}


quint8 SGET::query(const Obis &dlmsObj, const quint8 attribute, const ClassType classType, const Variant &data, Variant &result, SelectiveAccess::Type selectiveAccsess) {
    if (ConnectionStatus::DLMS_ESTABLISHED != core->status) {
        throw Error::DLMSConnectionNotOpen;
    }
    quint8 err = 0;
    QByteArray req;
    QByteArray res;

    req = handleRequest(dlmsObj, attribute, classType, data, selectiveAccsess);
    this->core->transfer(req, res);
    err = handleResponse(res, result);

    return err;
}

QByteArray SGET::requestGetNextDataBlock(int packageNum) {
    QByteArray result;
    result << (quint8)Command::GET_REQUEST;
    result << (quint8)DLMSGetRequest::NEXT;
    result << (quint8)0xC1;               // service id
    result << (quint32)packageNum;

    return result;
}

QByteArray SGET::handleRequest(const Obis &dlmsObj, const quint8 attribute, const ClassType classType, const Variant &data, SelectiveAccess::Type selectiveAccess) {
    QByteArray result;

    result << (quint8)Command::GET_REQUEST;
    result << (quint8)DLMSGetRequest::NORMAL;
    result << (quint8)0xC1;               // service id
    result << (quint8)0x00;               // invoke id
    result << (quint8)classType;
    result << dlmsObj.Data();
    result << (quint8)attribute;

    switch ((quint8)selectiveAccess) {
        case (quint8)SelectiveAccess::Type::NONE:
            result << (quint8)0x00;       // selective access disabled
            break;
        case (quint8)SelectiveAccess::Type::ByEntry:
        case (quint8)SelectiveAccess::Type::ByRange:
            result << (quint8)0x01;       // selective access enabled
            result << (quint8)selectiveAccess;
            break;
        default:
            break;
    }

    if (false == data.isNull()) {
        result << data.xdrData();
    }
    return result;
}

quint8 SGET::handleResponse(const QByteArray &data, Variant &result) {
    QByteArray::const_iterator it = data.begin();

    if ((quint8)Command::GET_RESPONSE != ReadUINT8(it, data)) {
        throw Error::InvalidMethod;
    }

    // UNDONE Необходимо реализовать метод получения данных WITH_LIST
    switch (ReadUINT8(it, data)) {
        case (quint8)DLMSGetResponse::NORMAL:
            ReadUINT8(it, data);
            return handleResponceNormal(it, data, result);
            break;
        case (quint8)DLMSGetResponse::WITH_DATABLOCK:
            ReadUINT8(it, data);
            return handleResponceWithBlock(data, result);
            break;
        case (quint8)DLMSGetResponse::WITH_LIST:
            throw Error::UnsupportedMethod;
        default:
            throw Error::InvalidMethod;
            break;
    }
}

quint8 SGET::handleResponceNormal(QByteArray::const_iterator &it, const QByteArray &data, Variant &result) {
    quint8 res = ReadUINT8(it,data);
    if (it < data.end() && 0 == res) {          // 0 - returned error code
        result = Variant::parseValue(data.mid(it  - data.begin(), data.end() - it));
        it = data.end();
    }
    return res;
}

quint8 SGET::handleResponceWithBlock(const QByteArray &data, Variant &result) {
    bool lastBlock = true;
    int packageNum = 0;
    quint8 res = 0;
    uint16_t size = 0;
    QByteArray response;
    QByteArray rxBuff = data;
    QByteArray::const_iterator rxIt;

    do {
        rxIt = rxBuff.cbegin();
        rxIt += 3;              // skip first 3 bytes

        lastBlock = (bool)ReadUINT8(rxIt, rxBuff);
        packageNum = ReadUINT32(rxIt, rxBuff);
        res = ReadUINT8(rxIt,rxBuff);
        size = ReadUINT8(rxIt, rxBuff);
        if (dataLenTag16 == size) {
            size = ReadUINT16(rxIt, rxBuff);
        } else if (dataLenTag8 == size) {
            size = ReadUINT8(rxIt, rxBuff);
        }
        response << rxBuff.right(size);

        if (false == lastBlock) {
            this->core->transfer(requestGetNextDataBlock(packageNum), rxBuff);
        }

    } while (!lastBlock);

    result = Variant::parseValue(response);
    return res;
}

}
