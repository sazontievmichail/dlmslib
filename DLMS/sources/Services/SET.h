#ifndef SET_H
#define SET_H

#include "DLMS/headers/Obis.h"
#include "DLMS/headers/Enums.h"
#include "DLMS/headers/Variant.h"
#include "DLMS/headers/IService.h"
#include "DLMS/headers/SelectiveAccess.h"

namespace DLMS {

enum class DLMSSetRequest : quint8 {
    NORMAL          = 1,
    WITH_DATABLOCK  = 2,
    WITH_LIST       = 3,
};

enum class DLMSSetResponse : quint8 {
    NORMAL          = 1,
    WITH_DATABLOCK  = 2,
    WITH_LIST       = 3,
};

class SSET : public IService {
public:
    SSET(Core &core) : IService(core) {};

    quint8 query(const Obis &dlmsObj
                 , const quint8 method
                 , const ClassType classType
                 , const Variant &data
                 , Variant &result
                 , SelectiveAccess::Type selectiveAccsess = SelectiveAccess::Type::NONE);

protected:
    QByteArray handleRequest(const Obis &dlmsObj, const quint8 attribute, const ClassType classType, const Variant &data,
                             SelectiveAccess::Type selectiveAccess = SelectiveAccess::Type::NONE);
    quint8 handleResponse(const QByteArray &data, Variant &result);
};


}

#endif // SET_H
