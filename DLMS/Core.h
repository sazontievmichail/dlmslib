#pragma once
#ifndef DLMS_H
#define DLMS_H

#include "DLMS/src/Services/IService.h"
#include "DLMS/src/Services/AARQ.h"


#include "HDLC/HDLCCore.h"
#include "HDLC/Enums.h"
#include "HDLC/io/iIO.h"
#include "HDLC/io/ioSerialPort.h"



namespace DLMS
{

class Core {

    enum MLRDLMSConnectionStatus{
        CONNECTION_STATUS_NO            = 0,
        CONNECTION_STATUS_PENDING       = 1,
        CONNECTION_STATUS_ESTABLISHED   = 2
    } ;

public:

    /**
     * @author      Michail Sazontev
     * @brief       Constructer
     */
    Core();

    void setIOSetting(const HDLC::IOSetting &ioSetting);
    const HDLC::IOSetting& getIOSetting() const;

    void setDLMSSetting(const HDLC::IOSetting &ioSetting);
    const Setting& getDLMSSetting() const;

    /**
     * @author      Michail Sazontev
     * @brief       Function initialie hdlc connection and open protected dlms connection
     *
     * @param[in]   dlmsSetting         The DLMS settings
     * @param[in]   ioSetting           The input/output setting
     * @param[in]   clientAddr          The client address 0x10 - public 0x20 - reader 0x30 - configurator
     * @param[in]   srvLogicalAddr      The server logical address
     * @param[in]   srvPhysicalAddr     The server physycal address
     *
     * @return      Connection status
     */
    ConnectionStatus Open(const Setting &dlmsSetting,
                          const HDLC::IOSetting &ioSetting, quint8 clientAddr, quint16 srvLogicalAddr, quint16 srvPhysicalAddr);

    /**
     * @author      Michail Sazontev
     * @brief       Function close hdlc connection
     */
    void Close();
    /**
     * @author      Michail Sazontev
     * @brief       Function return connection status
     *
     * @return      return connecton status
     */
    ConnectionStatus GetConnectionStatus() noexcept;

    /**
     * @author      Michail Sazontev
     * @brief       Function return connection status
     *
     * @return      return hdlc client
     */
    HDLC::HDLCCore& GetHDLC();

    ~Core();


protected:
    /**
     * @brief       Sending data and receiving a response
     * @author      Michail Sazontev
     *
     * @param[in]   txData      The data to transmit
     * @param[out]  rxData      The data to receive
     * @protected
     */
    void Transfer(const QByteArray &txData, QByteArray &rxData);

protected:
    Setting initDlmsSettings;
    Setting usingDlmsSettings;

    ConnectionStatus status;


private:
    quint8 clientAddr;
    quint16 srvLogicalAddr;
    quint16 srvPhysicalAddr;

    HDLC::HDLCCore hdlcClient;

    // Private service required to establish a connection
    SAARQ *AARQ;

public:
    // Public services
    IService *const Action;
    IService *const GET;
    IService *const SET;

    // The class has a bi-directional relationship with the service classes
    friend class SAARQ;
    friend class SAction;
    friend class SGET;
    friend class SSET;
};



}

#endif // DLMS_H
