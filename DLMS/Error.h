#pragma once
#ifndef DLMSERROR_H
#define DLMSERROR_H

#endif // DLMSERROR_H

#include <QCoreApplication>


namespace DLMS
{

/**
 * @brief The Error enum ошибки генерируемые DLMS модулем
 */

enum class Error : quint16 {
    Ok                                  = 0,
    InvalidData,
    InvalidMethod,
    InvalidSize,
    InvalidTag,
    InvalidType,
    InvalidDirection,
    InvalidSecurityMechanism,
    HDLCConnectionNotOpen,
    DLMSConnectionNotOpen,
    UnsupportedType,
    UnsupportedMethod,
    UnsupportedAuthenticationMethod,
    ExceptionUnknown,
    OutOfMemory,
};



}
