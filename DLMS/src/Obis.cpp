#include "src/Obis.h"

namespace DLMS
{


Obis::Obis() {
    memset(value, 0, sizeof(value));
}

Obis::Obis(quint8(&val)[6]) {
    memcpy_s(this->value, sizeof(this->value), val, sizeof(val));
}

Obis::Obis(const QByteArray &value) {
    unsigned int i = 0;
    for (auto it = value.begin(); it < value.end() && i < sizeof(this->value); it++, i++) {
        this->value[i] = *it;
    }
}

Obis::Obis(std::initializer_list<quint8> val) {
    unsigned int i = 0;
    for (auto it = val.begin(); it < val.end() && i < sizeof(this->value); it++, i++) {
        this->value[i] = *it;
    }
}

Obis::Obis(const Obis& val) {
    memcpy_s(this->value, sizeof(this->value), val.value, sizeof (val.value));
}

Obis& Obis::operator=(const Obis &val) {
    memcpy_s(this->value, sizeof(this->value), val.value, sizeof (val.value));
    return *this;
}

Obis& Obis::operator=(std::initializer_list<quint8> val) {
    unsigned int i = 0;
    for (auto it = val.begin(); it < val.end() && i < sizeof(value); it++, i++) {
        this->value[i] = *it;
    }
    return *this;
}

Obis& Obis::operator=(const QByteArray &val) {
    for (int i = 0; i < (int)sizeof(value) && i < val.size(); i++) {
        this->value[i] = val[i];
    }
    return *this;
}

bool operator>(const Obis &left, const Obis &right) {
    return (left.toNumber() > right.toNumber());
}

bool operator<(const Obis &left, const Obis &right) {
    return (left.toNumber() < right.toNumber());
}

bool operator==(const Obis &left, const std::initializer_list<qint16> &right) {
    unsigned int i = 0;
    for (auto it = right.begin(); it < right.end() && i < sizeof (left.value); it++, i++) {
        if (*it < 0) {
            continue;
        }
        if (*it != left.value[i]) {
            return false;
        }
    }
    return true;
}

quint8 Obis::getA() {
    return this->value[0];
}

quint8 Obis::getB() {
    return this->value[1];
}

quint8 Obis::getC() {
    return this->value[2];
}

quint8 Obis::getD() {
    return this->value[3];
}

quint8 Obis::getE() {
    return this->value[4];
}

quint8 Obis::getF() {
    return this->value[5];
}

QByteArray Obis::Data() const {
    return QByteArray::fromRawData((char*)value, 6);
}

quint64 Obis::toNumber() const {
    qint64 val;
    memcpy_s(&val, sizeof(val), value, sizeof(value));
    return val;
}

QString Obis::toString() const {
    QString val = "";
    for (int i = 0; i < (int)sizeof(value); i++) {
        val += QString::number((int)value[i]);
        if (i < (int)sizeof(value) - 1) {
            val += ".";
        }
    }
    return val;
}


}
