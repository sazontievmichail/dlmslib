#ifndef SELECTIVEACCESS_H
#define SELECTIVEACCESS_H

#include "Variant.h"

namespace DLMS {
namespace SelectiveAccess {

enum class Type : quint8 {
    NONE        = 0x00,
    ByRange     = 0x01,
    ByEntry     = 0x02,
};

//TODO  На данный момент ПУ не поддержвает captureObject, в дальнейшем при необходимости нужно будет добавить реализацию
/**
 * @brief toVariant             Selective access by Range
 *
 * @param[in] dataStart         The started value
 * @param[in] dataEnd           The end value
 * @param[in] captureObject     The capture object for select
 *
 * @return                      DLMS variant object
 */
Variant toVariant(const Variant &dataStart, const Variant &dataEnd, const Variant &captureObject);

//TODO  На данный момент ПУ не поддержвает startColumnEntry и stopColumnEntry, в дальнейшем при необходимости нужно будет добавить реализацию
/**
 * @brief toVariant             Selective access by Entry
 *
 * @param startEntry            The offset start entry
 * @param stopEntry             The offset end entry
 * @param startColumnEntry      The offset start column entry
 * @param stopColumnEntry       The offset end column entry
 *
 * @return                      DLMS variant object
 */
Variant toVariant(quint32 startEntry, quint32 stopEntry, quint16 startColumnEntry = 1, quint16 stopColumnEntry = 0);


}
};

#endif // SELECTIVEACCESS_H
