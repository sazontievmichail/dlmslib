#include "DLMS/src/Services/CGET.h"
#include "DLMS/DLMSCore.h"

/*
namespace DLMS
{

ActioGet::ActioGet(DLMSCore &core)
{
    this->core = &core;
}

quint8 ActioGet::Query(const DLMSObject &dlmsObj, const quint8 method, const DLMSClassType classType, const DLMSVariant &data, DLMSVariant &result)
{
    quint8 err = 0;
    QByteArray req;
    QByteArray res;

    req = GenerateRequest(dlmsObj, method, classType, data);
    this->core->Transfer(req, res);
    err = ParseResponse(res, result);

    return err;
}

QByteArray ActioGet::GenerateRequest(const DLMSObject &dlmsObj, const quint8 attribute, const DLMSClassType classType, const DLMSVariant &data)
{
    QByteArray result;

    result<<(quint8)DLMSCommand::GET_REQUEST;
    result<<(quint8)DLMSGetRequest::NORMAL;
    result<<(quint8)0xC1;
    result<<(quint8)0x00;
    result<<(quint8)classType;
    result<<dlmsObj.Data();
    result<<(quint8)attribute;

    if(data.IsNull())
    {
        result<<(quint8)0x00;
    }
    else
    {
        result<<(quint8)0x01;
        result<<data.XdrData();
    }

    return result;
}

quint8 ActioGet::ParseResponse(const QByteArray &data, DLMSVariant &result)
{
    QByteArray::const_iterator it = data.begin();

    if((quint8)DLMSCommand::GET_RESPONSE != (quint8)*it){
        throw DLMSException(ExceptionCodes::InvalidTag, DLMSModule::GET);
    }
    switch(*++it)
    {
    case (quint8)DMSGetResponse::NORMAL:
        return ParseResponceNormal(it += 3, data, result);
        break;
    default:
        throw DLMSException(ExceptionCodes::UnsupportedMethod, DLMSModule::GET);
        break;
    }

}

quint8 ActioGet::ParseResponceNormal(QByteArray::const_iterator &it, const QByteArray &data, DLMSVariant &result)
{
    quint8 res = *it;
    if((++it) < data.end()){
        result = data.mid(it - data.begin(), data.end() - it);
        it = data.end();
    }
    return res;
}




}
*/
