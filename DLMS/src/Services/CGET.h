#pragma once
#ifndef GET_H
#define GET_H

#include <QCoreApplication>


//#include "DLMS/src/Services/IService.h"
//#include "DLMS/src/DLMSEnums.h"
//#include "DLMS/src/DLMSObject.h"
//#include "DLMS/src/DLMSVariant.h"


namespace DLMS
{
class DLMSCore;

/*
enum class DLMSGetRequest : quint8
{
    NORMAL      = 1,
    NEXT        = 2,
    WITH_LIST   = 3
} ;

enum class DMSGetResponse : quint8
{
    NORMAL              = 1,
    WITH_DATABLOCK      = 2,
    WITH_LIST           = 3,
};
*/


class ActioGet
{
public:
    ActioGet(DLMSCore &core);
};

/*
class ActioGet
{
public:
    ActioGet(DLMSCore &core);
    quint8 Query(const DLMSObject &dlmsObj, const quint8 method, const DLMSClassType classType, const DLMSVariant &data, DLMSVariant &result);

protected:
    QByteArray GenerateRequest(const DLMSObject &dlmsObj, const quint8 attribute, const DLMSClassType classType, const DLMSVariant &data);
    quint8 ParseResponse(const QByteArray &data, DLMSVariant &result);

private:
    quint8 ParseResponceNormal(QByteArray::const_iterator &it, const QByteArray &data, DLMSVariant &result);

    DLMSCore *core;
};
*/
}



#endif // GET_H
