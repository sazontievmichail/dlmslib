#pragma once
#ifndef ISERVICE_H
#define ISERVICE_H

#include "DLMS/src/SelectiveAccess.h"
#include "DLMS/src/Variant.h"
#include "DLMS/src/Obis.h"
#include "DLMS/Setting.h"


namespace DLMS
{
class Core;

class IService {
public:
    IService(Core &core) {
        this->core = &core;
    }
    IService(IService &service) {
        this->core = service.core;
    }

    virtual quint8 Query(const Obis &dlmsObj
                         , const quint8 method
                         , const ClassType classType
                         , const Variant &data, Variant &result
                         , SelectiveAccess::Type selectiveAccess = SelectiveAccess::Type::NONE) = 0;

    virtual ~IService() {
        this->core = nullptr;
    }
protected:
    QByteArray HandleRequest(const Obis &dlmsObj, const quint8 attribute, const ClassType classType, const Variant &data, SelectiveAccess::Type selectiveAccess);
    quint8 HandleResponse(const QByteArray &data, Variant &result);

protected:
    Core *core;
};


}

#endif // ISERVICE_H
