#include "DLMS/src/Services/GET.h"
#include "DLMS/src/Utils.h"
#include "DLMS/Core.h"

#include <iostream>

namespace DLMS
{


quint8 SGET::Query(const Obis &dlmsObj
                          , const quint8 attribute
                          , const ClassType classType
                          , const Variant &data
                          , Variant &result
                          , SelectiveAccess::Type selectiveAccsess) {
    if (ConnectionStatus::DLMS_ESTABLISHED != core->status) {
        throw Error::DLMSConnectionNotOpen;
    }
    quint8 err = 0;
    QByteArray req;
    QByteArray res;

    req = HandleRequest(dlmsObj, attribute, classType, data, selectiveAccsess);
    this->core->Transfer(req, res);
    err = HandleResponse(res, result);

    return err;
}

QByteArray SGET::RequestGetNextDataBlock(int packageNum) {
    QByteArray result;
    result<<(quint8)Command::GET_REQUEST;
    result<<(quint8)DLMSGetRequest::NEXT;
    result<<(quint8)0xC1;
    result<<(quint32)packageNum;

    return result;
}

QByteArray SGET::HandleRequest(const Obis &dlmsObj, const quint8 attribute, const ClassType classType, const Variant &data, SelectiveAccess::Type selectiveAccess) {
    QByteArray result;

    result<<(quint8)Command::GET_REQUEST;
    result<<(quint8)DLMSGetRequest::NORMAL;
    result<<(quint8)0xC1;
    result<<(quint8)0x00;
    result<<(quint8)classType;
    result<<dlmsObj.Data();
    result<<(quint8)attribute;

    switch ((quint8)selectiveAccess) {
    case (quint8)SelectiveAccess::Type::NONE:
        result<<(quint8)0x00;
        break;
    case (quint8)SelectiveAccess::Type::ByEntry:
    case (quint8)SelectiveAccess::Type::ByRange:
        result<<(quint8)0x01;
        result<<(quint8)selectiveAccess;
        break;
    default:
        break;
    }

    if (false == data.isNull()) {
        result<<data.XdrData();
    }


    return result;
}

quint8 SGET::HandleResponse(const QByteArray &data, Variant &result) {
    QByteArray::const_iterator it = data.begin();

    if ((quint8)Command::GET_RESPONSE != ReadUINT8(it, data)) {
        throw Error::InvalidMethod;
    }

    // UNDONE Необходимо реализовать метод получения данных WITH_LIST
    switch (ReadUINT8(it, data)) {
    case (quint8)DLMSGetResponse::NORMAL:
        ReadUINT8(it, data);
        return HandleResponceNormal(it, data, result);
        break;
    case (quint8)DLMSGetResponse::WITH_DATABLOCK:
        ReadUINT8(it, data);
        return HandleResponceWithBlock(data, result);
        break;
    case (quint8)DLMSGetResponse::WITH_LIST:
        throw Error::UnsupportedMethod;
    default:
        throw Error::InvalidMethod;
        break;
    }
}

quint8 SGET::HandleResponceNormal(QByteArray::const_iterator &it, const QByteArray &data, Variant &result) {
    quint8 res = ReadUINT8(it,data);
    if (it < data.end() && 0 == res) {
        result = Variant::ParseValue(data.mid(it  - data.begin(), data.end() - it));
        it = data.end();
    }
    return res;
}

quint8 SGET::HandleResponceWithBlock(const QByteArray &data, Variant &result) {
    bool lastBlock = true;
    int packageNum = 0;
    quint8 res = 0;
    uint16_t size = 0;
    QByteArray response;
    QByteArray rxBuff = data;
    QByteArray::const_iterator rxIt;

    do {
        rxIt = rxBuff.cbegin();
        rxIt += 3;

        lastBlock = (bool)ReadUINT8(rxIt, rxBuff);
        packageNum = ReadUINT32(rxIt, rxBuff);
        res = ReadUINT8(rxIt,rxBuff);
        size = ReadUINT8(rxIt, rxBuff);
        if (0x82 == size) {
            size = ReadUINT16(rxIt, rxBuff);
        }
        else if (0x81 == size) {
            size = ReadUINT8(rxIt, rxBuff);
        }
        response<<rxBuff.right(size);

        if (false == lastBlock) {
            this->core->Transfer(RequestGetNextDataBlock(packageNum), rxBuff);
        }

    } while (!lastBlock);

    result = Variant::ParseValue(response);
    return res;
}

}
