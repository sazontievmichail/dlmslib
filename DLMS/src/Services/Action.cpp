#include "DLMS/src/Services/Action.h"
#include "DLMS/src/Utils.h"
#include "DLMS/Core.h"


namespace DLMS
{


QByteArray SAction::HandleRequest(const Obis &dlmsObj, const quint8 method, const ClassType classType, const Variant &data) {
    QByteArray result;

    result.clear();
    result<<(quint8)Command::METHOD_REQUEST;
    result<<(quint8)DLMSActionRequest::NORMAL;
    result<<(quint8)0xC1;
    result<<(quint8)0x00;
    result<<(quint8)classType;
    result<<dlmsObj.Data();
    result<<(quint8)method;

    if (data.isNull()) {
        result<<(quint8)0x00;
    }
    else {
        result<<(quint8)0x01;
        result<<data.XdrData();
    }
    return result;
}

quint8 SAction::HandleResponse(const QByteArray &data, Variant &result) {
    QByteArray::const_iterator it = data.begin();

    if ((quint8)Command::METHOD_RESPONSE != ReadUINT8(it, data)) {
        throw Error::InvalidMethod;
    }

    // TODO Не реализованно получение данных типа NEXT_PBLOCK
    // TODO Не реализованно получение данных типа WITH_LIST
    switch (ReadUINT8(it, data)) {
    case (quint8)DLMSActionRequest::NORMAL:
        break;
    case (quint8)DLMSActionRequest::NEXT_PBLOCK:
    case (quint8)DLMSActionRequest::WITH_PBLOCK:
    case (quint8)DLMSActionRequest::WITH_LIST:
    case (quint8)DLMSActionRequest::WITH_FIRST_PBLOCK:
    case (quint8)DLMSActionRequest::WITH_LIST_AND_FIRST_PBLOCK:
        throw Error::UnsupportedMethod;
    default:
        throw Error::InvalidMethod;
        break;
    }

    ReadUINT8(it, data); // C1

    quint8 res =  ReadUINT8(it, data);
    if (res != 0x00) {
        return res;
    }

    if (0x01 == ReadUINT8(it, data)) {
        result = Variant::ParseValue(data.right(data.end() - (it + 1)));
    }
    return res;
}

quint8 SAction::Query(const Obis &dlmsObj
                      , const quint8 method
                      , const ClassType classType
                      , const Variant &data
                      , Variant &result
                      , SelectiveAccess::Type) {
    if (ConnectionStatus::DLMS_PENDING > core->status) {
        throw Error::DLMSConnectionNotOpen;
    }
    quint8 err = 0;
    QByteArray tmp;
    QByteArray res;

    tmp = HandleRequest(dlmsObj, method, classType, data);
    this->core->Transfer(tmp, res);
    err = HandleResponse(res, result);

    return err;
}



}
