#pragma once
#ifndef ACTION_H
#pragma once
#define ACTION_H


#include "DLMS/src/Services/IService.h"


namespace DLMS
{

class Core;

enum class DLMSActionRequest : quint8 {
    NORMAL                          = 1,
    /* Поддерживаетс только NORMAL */
    NEXT_PBLOCK,
    WITH_LIST,
    WITH_FIRST_PBLOCK,
    WITH_LIST_AND_FIRST_PBLOCK,
    WITH_PBLOCK
};


class SAction : public IService {
public:
    SAction(Core &core) : IService(core) {};

    quint8 Query(const Obis &dlmsObj
                 , const quint8 method
                 , const ClassType classType
                 , const Variant &data
                 , Variant &result
                 , SelectiveAccess::Type selectiveAccsess = SelectiveAccess::Type::NONE);

protected:
    QByteArray HandleRequest(const Obis &dlmsObj, const quint8 method, const ClassType classType, const Variant &data);
    quint8 HandleResponse(const QByteArray &data, Variant &result);
};

}

#endif // ACTION_H
