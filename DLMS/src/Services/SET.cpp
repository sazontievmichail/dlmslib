#include "DLMS/src/Services/SET.h"
#include "DLMS/src/Utils.h"
#include "DLMS/Core.h"


namespace DLMS
{



quint8 SSET::Query(const Obis &dlmsObj
                          , const quint8 attribute
                          , const ClassType classType
                          , const Variant &data
                          , Variant &result
                          , SelectiveAccess::Type) {
    if (ConnectionStatus::DLMS_ESTABLISHED != core->status) {
        throw Error::DLMSConnectionNotOpen;
    }
    quint8 err = 0;
    QByteArray req;
    QByteArray res;

    req = HandleRequest(dlmsObj, attribute, classType, data);
    this->core->Transfer(req, res);
    err = HandleResponse(res, result);

    return err;
}

QByteArray SSET::HandleRequest(const Obis &dlmsObj, const quint8 attribute, const ClassType classType, const Variant &data) {
    QByteArray result;
    result<<(quint8)Command::SET_REQUEST;
    result<<(quint8)DLMSSetRequest::NORMAL;
    result<<(quint8)0xC1;
    result<<(quint8)0x00;
    result<<(quint8)classType;
    result<<dlmsObj.Data();
    result<<(quint8)attribute;

    result<<(quint8)0x00;
    result<<data.XdrData();

    return result;
}

quint8 SSET::HandleResponse(const QByteArray &data, Variant &result)
{
    QByteArray::const_iterator it = data.begin();

    if ((quint8)Command::SET_RESPONSE != ReadUINT8(it, data)) {
        throw Error::InvalidMethod;
    }
    switch (ReadUINT8(it, data)) {
    case (quint8)DLMSSetResponse::NORMAL:
        ReadUINT8(it, data);
        result.Clear();
        return ReadUINT8(it, data);
        break;
    case (quint8)DLMSSetResponse::WITH_DATABLOCK:
    case (quint8)DLMSSetResponse::WITH_LIST:
        throw Error::UnsupportedMethod;
    default:
        throw Error::InvalidMethod;
        break;
    }

}



}
