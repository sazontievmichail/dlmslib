#pragma once
#ifndef GET_H
#define GET_H


#include "DLMS/src/Services/IService.h"



namespace DLMS
{

enum class DLMSGetRequest : quint8 {
    NORMAL      = 1,
    NEXT        = 2,
    WITH_LIST   = 3
} ;

enum class DLMSGetResponse : quint8 {
    NORMAL              = 1,
    WITH_DATABLOCK      = 2,
    WITH_LIST           = 3,
};


class SGET : public IService {
public:
    SGET(Core &core) : IService(core) {};

    quint8 Query(const Obis &dlmsObj
                 , const quint8 method
                 , const ClassType classType
                 , const Variant &data
                 , Variant &result
                 , SelectiveAccess::Type selectiveAccsess = SelectiveAccess::Type::NONE);


protected:
    QByteArray HandleRequest(const Obis &dlmsObj, const quint8 attribute, const ClassType classType, const Variant &data, SelectiveAccess::Type selectiveAccess);
    quint8 HandleResponse(const QByteArray &data, Variant &result);

private:
    QByteArray RequestGetNextDataBlock(int packageNum);
    quint8 HandleResponceNormal(QByteArray::const_iterator &it, const QByteArray &data, Variant &result);
    quint8 HandleResponceWithBlock(const QByteArray &data, Variant &result);
};



}


#endif // GET_H
