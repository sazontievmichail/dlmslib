#pragma once
#ifndef SET_H
#define SET_H


#include "DLMS/src/Services/IService.h"



namespace DLMS
{

enum class DLMSSetRequest : quint8 {
    NORMAL          = 1,
    WITH_DATABLOCK  = 2,
    WITH_LIST       = 3,
};

enum class DLMSSetResponse : quint8 {
    NORMAL          = 1,
    WITH_DATABLOCK  = 2,
    WITH_LIST       = 3,
};



class SSET : public IService {
public:
    SSET(Core &core) : IService(core) {};

    quint8 Query(const Obis &dlmsObj
                 , const quint8 method
                 , const ClassType classType
                 , const Variant &data
                 , Variant &result
                 , SelectiveAccess::Type selectiveAccsess = SelectiveAccess::Type::NONE);


protected:
    QByteArray HandleRequest(const Obis &dlmsObj, const quint8 attribute, const ClassType classType, const Variant &data);
    quint8 HandleResponse(const QByteArray &data, Variant &result);

};


}

#endif // SET_H
