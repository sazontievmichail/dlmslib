#include "AARQ.h"
#include "DLMS/Core.h"
#include "DLMS/src/Utils.h"

#include <QCoreApplication>


namespace DLMS
{


SAARQ::SAARQ(Core &core) {
    authenticationResult.assotiationResult = AssotiationResult::DLMS_ASSOCIATION_RESULT_ACCEPTED;
    authenticationResult.sourceDiagnostic = SourceDiagnostic::NONE;
    authenticationResult.confirmedServiceError = ConfirmedServiceError::NONE;
    authenticationResult.serviceError = ServiceError::NONE;

    this->core = &core;
};


void SAARQ::HandleRequest(const Setting &setting, Setting &severSetting, QByteArray &data) {
    data.clear();
    data<<(quint8)(BERTypes::APPLICATION | BERTypes::CONSTRUCTED);
    data<<(quint8)0x00;

    GenerateApplicationContextName(setting, data);
    GenerateAuthenticationData(setting, severSetting, data);
    GenerateUserInformation(setting, data);

    data[1] = quint8(data.size() - 2);
}

void SAARQ::HandleResponse(Setting &setting, QByteArray &data) {
    QByteArray::const_iterator it = data.cbegin();

    //Parse Header
    if ((BERTypes::APPLICATION | BERTypes::CONSTRUCTED | APDUType::APPLICATION_CONTEXT_NAME) != ReadUINT8(it, data)) {
        throw  Error::UnsupportedMethod;
    }

    quint32 len = 0;
    GetObjectCount(it, data, len);

    if (quint32(data.end() - it) != len) {
        throw  Error::InvalidSize;
    }

    //Parse Data
    quint8 flag = 0x00;
    do {

        flag = ReadUINT8(it, data);

        switch (flag) {
        case BERTypes::CONTEXT | BERTypes::CONSTRUCTED | APDUType::APPLICATION_CONTEXT_NAME:          // A1
            ParseAplicationContextName(setting, it, data);
            break;
        case BERTypes::CONTEXT | BERTypes::CONSTRUCTED | APDUType::CALLED_AP_TITLE:                   // A2
            ParseAssociationResult(it, data);
            break;
        case BERTypes::CONTEXT | BERTypes::CONSTRUCTED | APDUType::CALLED_AE_QUALIFIER:               // A3
            ParseSourceDiagnostic(it, data);
            break;
        case BERTypes::CONTEXT | BERTypes::CONSTRUCTED | APDUType::SENDER_ACSE_REQUIREMENTS:          // AA
            ParseAuthenticationData(setting, it, data);
            break;
        case BERTypes::CONTEXT | BERTypes::CONSTRUCTED | APDUType::USER_INFORMATION:                  // BE
            ParseUserInformation(setting, it, data);
            break;
        case BERTypes::CONTEXT | (quint8)APDUType::CALLING_AP_INVOCATION_ID:                           // 88
        case BERTypes::CONTEXT | (quint8)APDUType::CALLING_AE_INVOCATION_ID:                           // 89
            ParseAuthenticationFunctional(setting, it, data);
            break;
        case BERTypes::CONTEXT | BERTypes::CONSTRUCTED | APDUType::CALLED_AP_INVOCATION_ID:           // A4
        case BERTypes::CONTEXT | BERTypes::CONSTRUCTED | APDUType::CALLED_AE_INVOCATION_ID:           // A5
        case BERTypes::CONTEXT | BERTypes::CONSTRUCTED | APDUType::CALLING_AP_TITLE:                  // A6
        case BERTypes::CONTEXT | BERTypes::CONSTRUCTED | 7:                                           // A7
        case BERTypes::CONTEXT | BERTypes::CONSTRUCTED | APDUType::CALLING_AP_INVOCATION_ID:          // A8
        case BERTypes::CONTEXT | BERTypes::CONSTRUCTED | APDUType::CALLING_AE_INVOCATION_ID:          // A9
        case BERTypes::CONTEXT | BERTypes::CONSTRUCTED | APDUType::CALLING_AUTHENTICATION_VALUE:      // AC
        case BERTypes::CONTEXT | (quint8)APDUType::SENDER_ACSE_REQUIREMENTS:                           // 8A
        case BERTypes::CONTEXT | (quint8)APDUType::MECHANISM_NAME:                                     // 8B
        case BERTypes::CONTEXT:                                                                        // 80
            SkipTag(it, data);
            break;
        default:
            throw  Error::InvalidTag;
        }

        it++;
    } while (it < data.cend());
}

quint8 SAARQ::Querry(const Setting &setting, Setting &serverSetting) {
    QByteArray data;
    QByteArray res;
    serverSetting = setting;
    HandleRequest(setting, serverSetting, data);
    this->core->Transfer(data, res);
    HandleResponse(serverSetting, res);

    if (AssotiationResult::DLMS_ASSOCIATION_RESULT_ACCEPTED != authenticationResult.assotiationResult) {
        return (quint8)authenticationResult.assotiationResult;
    }

    if (Authentication::HIGH > core->usingDlmsSettings.authentication) {
        core->status = ConnectionStatus::DLMS_ESTABLISHED;
    }
    else {
        core->status = ConnectionStatus::DLMS_PENDING;
    }

    return 0;
}


QByteArray SAARQ::GenerateCtoSChallenge(quint16 len) {
    QByteArray res;
    res.resize(len);
    for(auto &val : res) {
        val = (quint8)std::rand();
    }
    return res;
}

void SAARQ::GenerateApplicationContextName(const Setting &setting, QByteArray &data) {
    /*
     *  Не реализовано проставление протокола передачи данных
     */


    // Идентификатор объекта
    data<<(quint8)(BERTypes::CONTEXT | BERTypes::CONSTRUCTED | APDUType::APPLICATION_CONTEXT_NAME);       // Flag 0xA1
    data<<(quint8)0x09;                                                                                     // Len
    data<<(quint8)BERTypes::OBJECT_IDENTIFIER;                                                             // 0x06
    data<<(quint8)0x07;                                                                                     // Len

    data<<(quint8)0x60;
    data<<(quint8)0x85;
    data<<(quint8)0x74;
    data<<(quint8)0x05;
    data<<(quint8)0x08;
    data<<(quint8)0x01;

    data<<(quint8) (setting.useLongNameReferencing ? 0x01 : 0x00);
}

void SAARQ::GenerateAuthenticationData(const Setting &setting, Setting &serverSetting, QByteArray &data) {
    serverSetting.cToS = GenerateCtoSChallenge(16);

    switch ((quint8)setting.authentication) {
    case (quint8)Authentication::HIGH_ECDSA:
    case (quint8)Authentication::HIGH_GMAC:
    case (quint8)Authentication::HIGH_MD5:
    case (quint8)Authentication::HIGH_SHA1:
    case (quint8)Authentication::HIGH_SHA256:
        throw  Error::UnsupportedAuthenticationMethod;
        break;
    case (quint8)Authentication::NONE:
        break;
    case (quint8)Authentication::LOW:
    case (quint8)Authentication::HIGH:
        //8A 02 07 80 8B 07
        data<<(quint8)(BERTypes::CONTEXT | (quint8)APDUType::SENDER_ACSE_REQUIREMENTS);
        data<<(quint8)0x02;
        data<<(quint8)(BERTypes::BIT_STRING | BERTypes::OCTET_STRING);
        data<<(quint8)0x80;
        data<<(quint8)(BERTypes::CONTEXT | (quint8)APDUType::MECHANISM_NAME);
        data<<(quint8)0x07;

        //Mechanism 60 85 74 05 08 02 01 01(05)
        data<<(quint8)0x60;
        data<<(quint8)0x85;
        data<<(quint8)0x74;
        data<<(quint8)0x05;
        data<<(quint8)0x08;
        data<<(quint8)0x02;
        data<<(quint8)setting.authentication;

        QByteArray sequence = (Authentication::HIGH == setting.authentication) ? serverSetting.cToS : QByteArray::fromHex(serverSetting.authenticationKey.toLatin1());
        if (Authentication::HIGH == setting.authentication) {
            sequence = serverSetting.cToS;
        }
        else {
            sequence = serverSetting.authenticationKey.toLatin1();
        }



        data<<(quint8)(BERTypes::CONTEXT | BERTypes::CONSTRUCTED | APDUType::CALLING_AUTHENTICATION_VALUE);
        data<<(quint8)(sequence.size() + 2);
        data<<(quint8)BERTypes::CONTEXT;
        data<<(quint8)sequence.size();
        data<<sequence;
        break;
    }
}
void SAARQ::GenerateUserInformation(const Setting &setting, QByteArray &data) {
    /*
     * Не поддерживается шифрование данных
     */
    data<<(quint8)(BERTypes::CONTEXT | BERTypes::CONSTRUCTED | APDUType::USER_INFORMATION);
    data<<(quint8)0x10;
    data<<(quint8)BERTypes::OCTET_STRING;
    data<<(quint8)0x0E;

    data<<(quint8)Command::INITIATE_REQUEST;
    data<<(quint8)0x00;
    data<<(quint8)0x00;
    data<<(quint8)0x00;
    data<<setting.dlmsVersion;
    data<<(quint8)0x5F;
    data<<(quint8)0x1F;
    data<<(quint8)0x04;
    data<<(quint8)0x00;

    data<<(quint8)((setting.conformance >> 16) & 0xff);
    data<<(quint8)((setting.conformance >> 8) & 0xff);
    data<<(quint8)(setting.conformance & 0xff);
    data<<setting.maxApduSize;
}

void SAARQ::ParseAplicationContextName(Setting &setting, QByteArray::const_iterator &it, const QByteArray &data) {
    const quint8 reviewArr[] = {0x09, 0x06, 0x07, 0x60, 0x85, 0x74, 0x05, 0x08, 0x01};
    QByteArray review((char*)reviewArr, sizeof(reviewArr));

    if ((review.size() + 1) > (data.cend() - it)) {
        throw Error::InvalidSize;
    }
    if (review != data.mid(it - data.cbegin(), review.size())) {
        throw Error::InvalidSecurityMechanism;
    }


    it += review.size();
    if (0x01 == *it || 0x02 == *it) {
        setting.useLongNameReferencing = (*it == 0x01) ? true : false;
    }
    else {
        throw Error::InvalidSecurityMechanism;
    }
}

void SAARQ::ParseAssociationResult(QByteArray::const_iterator &it, const QByteArray &data) {
    CheckTagLen(it, data);


    quint8 reviewArr[] = {0x03, 0x02, 0x01};
    QByteArray review = QByteArray((char*)reviewArr, 3);
    if (review != data.mid(it - data.cbegin(), 3)) {
        throw  Error::InvalidTag;
    }

    it+= review.size();
    authenticationResult.assotiationResult = static_cast<AssotiationResult>(*it);
}

void SAARQ::ParseSourceDiagnostic(QByteArray::const_iterator &it, const QByteArray &data) {
    CheckTagLen(it, data);


    quint8 reviewArr[] = {0x05, 0xA1, 0x03, 0x02, 0x01};
    QByteArray review = QByteArray((char*)reviewArr, 5);

    if (review != data.mid(it - data.cbegin(), 5)) {
         throw  Error::InvalidTag;
    }
    it += review.size();
    authenticationResult.sourceDiagnostic = static_cast<SourceDiagnostic>(*it);
}

void SAARQ::ParseAuthenticationData(Setting &setting, QByteArray::const_iterator &it, const QByteArray &data) {
    CheckTagLen(it, data);

    if (0x80 != (quint8)*++it) {
        throw  Error::InvalidTag;
    }
    quint8 len = *++it;
    setting.sToC.clear();
    setting.sToC = data.mid((++it) - data.cbegin(), len);
    it += len - 1;
}

void SAARQ::ParseUserInformation(Setting &setting, QByteArray::const_iterator &it, const QByteArray &data) {
    CheckTagLen(it, data);
    ReadUINT8(it, data);    // skip tag len

    if (BERTypes::OCTET_STRING != ReadUINT8(it, data)) {
        throw  Error::InvalidType;
    }

    CheckTagLen(it, data);
    ReadUINT8(it, data);

    switch(ReadUINT8(it, data)) {
    case (quint8) Command::INITIATE_RESPONSE:
        if (0 != ReadUINT8(it, data)) {
            ReadUINT8(it, data);       //quality of service не поддерживается, пропускаем
        }
        break;
    case (quint8) Command::CONFIRMED_SERVICE_ERROR:
        authenticationResult.confirmedServiceError = static_cast<ConfirmedServiceError>(ReadUINT8(it, data));
        authenticationResult.serviceError = static_cast<ServiceError>(ReadUINT8(it, data));
        ReadUINT8(it, data);
        return;
        break;
    default:
        throw  Error::InvalidTag;
        break;
    }

    setting.dlmsVersion = ReadUINT8(it, data);
    if (0x5F != ReadUINT8(it, data) || 0x1F != ReadUINT8(it, data)){
       throw  Error::InvalidTag;
    }
    ReadUINT8(it, data);

    setting.conformance = ReadUINT32(it, data);
    setting.maxApduSize = ReadUINT16(it, data);

    if (0x0007 != ReadUINT16(it, data))
    {
        throw Error::InvalidTag;
    }

}

void SAARQ::ParseAuthenticationFunctional(Setting &setting, QByteArray::const_iterator &it, const QByteArray &data) {
    const quint8 reviewArr[] = {0x02, 0x07, 0x80, 0x89, 0x07, 0x60, 0x85, 0x74, 0x05, 0x08, 0x02};
    QByteArray review((char*)reviewArr, 11);

    if ((11 + 1) > (data.cend() - it)) {
        throw  Error::InvalidSize;
    }
    if (review != data.mid(it - data.cbegin(), 11)) {
        throw  Error::InvalidTag;
    }

    it+= review.size();
    setting.authentication = static_cast<Authentication>(*it);
}

void SAARQ::SkipTag(QByteArray::const_iterator &it, const QByteArray &data) {
    CheckTagLen(it, data);
    it += *it;
}

void SAARQ::CheckTagLen(QByteArray::const_iterator &it, const QByteArray &data) {
    if (it == data.cend() || *it > data.cend() - it) {
        throw  Error::InvalidSize;
    }
}

void SAARQ::GetObjectCount(QByteArray::const_iterator &it, const QByteArray &data, quint32 &count) {
    quint8 size = ReadUINT8(it, data);
    if (0x81 == size) {
        count = ReadUINT8(it, data);
    }
    else if (0x82 == size) {
        count = ReadUINT16(it, data);
    }
    else if (0x84 == size) {
        count = ReadUINT32(it, data);
    }
    else {
        count = size;
    }
}

}
