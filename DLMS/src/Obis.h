#pragma once
#ifndef DLMSOBIS_H
#define DLMSOBIS_H

#include <QCoreApplication>
#include <QByteArray>


namespace DLMS
{


class Obis {
public:
    Obis();
    Obis(quint8(&val)[6]);
    Obis(const QByteArray&);
    Obis(std::initializer_list<quint8> val);
    Obis(const Obis& val);

    Obis& operator=(const Obis &val);
    Obis& operator=(std::initializer_list<quint8> val);
    Obis& operator=(const QByteArray &val);

    friend bool operator>(const Obis &left, const Obis &right);
    friend bool operator<(const Obis &left, const Obis &right);
    friend bool operator==(const Obis &left, const std::initializer_list<qint16> &right);

    quint8 getA();
    quint8 getB();
    quint8 getC();
    quint8 getD();
    quint8 getE();
    quint8 getF();

    QByteArray Data() const;

    quint64 toNumber() const;
    QString toString() const;

protected:
    quint8 value[6];
};


}

#endif // DLMSOBIS_H
