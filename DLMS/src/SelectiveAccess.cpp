#include "SelectiveAccess.h"


namespace DLMS {
namespace SelectiveAccess {

Variant toVariant(const Variant &dataStart, const Variant &dataEnd, const Variant &captureObject) {
    Variant res((QVector<Variant>){captureObject, dataStart, dataEnd, DLMS::Variant(QVector<Variant>())}, true);

    return res;
}

Variant toVariant(quint32 startEntry, quint32 stopEntry, quint16 startColumnEntry, quint16 stopColumnEntry) {
    Variant res((QVector<Variant>){
                    Variant(startEntry)
                    , Variant(stopEntry)
                    , Variant(startColumnEntry )
                    , Variant(stopColumnEntry)
                });

    return res;
}


}
};
