#pragma once
#ifndef DLMSVARIANT_H
#define DLMSVARIANT_H

#include <QMetaType>
#include <QByteArray>
#include <QDateTime>

#include "../Enums.h"
#include "../Error.h"
#include "Obis.h"


namespace DLMS
{


/**
 * @author  Michail Sazontev
 * @brief   Универсальный тип данных используемый в ходе обмена данными между клиентом и сервером
 *          Используется для упаковки базовых типов в модель XDR и обратно
 */
class Variant {
private:
    DataType type;

    QByteArray data;                // for octet string
    qint64 intVal;                  // for signed
    quint64 uintVal;                // for unsigned
    float floatVal;                 // for float
    double doubleVal;               // for double
    bool boolVal;                   // for bool

    QVector<Variant> listVal;     // for structure or array
public:
    Variant();
    Variant(bool    value);
    Variant(float   value);
    Variant(double  value);
    Variant(quint8  value);
    Variant(quint16 value);
    Variant(quint32 value);
    Variant(quint64 value);
    Variant(qint8   value);
    Variant(qint16  value);
    Variant(qint32  value);
    Variant(qint64  value);
    Variant(const QByteArray&);
    Variant(const QVector<Variant>&, bool isStruct = false);
    Variant(const QDateTime dateTime, quint16 deviation = 0xff4c, quint8 status = 0x00);

    bool isNull() const;
    bool isArray() const;
    bool isStruct() const;
    bool isNumber() const;
    bool isBoolean() const;
    bool isOctetString() const;
    bool isObis() const;
    bool isDateTime() const;

    qint64 toNumber() const;
    QString toString() const;
    bool toBoolean() const;
    QByteArray toOctetString() const;
    QString toXML() const;
    Obis toObis() const;
    QDateTime toDateTime() const;


    Variant& operator= (bool);
    Variant& operator= (quint8);
    Variant& operator= (quint16);
    Variant& operator= (quint32);
    Variant& operator= (quint64);
    Variant& operator= (qint8);
    Variant& operator= (qint16);
    Variant& operator= (qint32);
    Variant& operator= (qint64);
    Variant& operator= (const QByteArray&);
    Variant& operator= (const QVector<Variant>&);
    Variant& operator[] (quint32);


    bool Add(const Variant&);
    int Size() const;

    const QByteArray& Data() const;
    QByteArray XdrData() const;


    static Variant ParseValue(const QByteArray &data);

    void Clear();


    ~Variant();
private:
    static void CheckTagLen(QByteArray::const_iterator &it, const QByteArray &data);
    static Variant Parse(QByteArray::const_iterator &it, const QByteArray &data);
    static quint16 GetTagLen(QByteArray::const_iterator &it, const QByteArray &data);
};

}

Q_DECLARE_METATYPE(DLMS::Variant);


#endif // DLMSVARIANT_H
