#include "DLMS/src/Services/AARQ.h"
#include "DLMS/src/Services/Action.h"
#include "DLMS/src/Services/SET.h"
#include "DLMS/src/Services/GET.h"
#include "DLMS/src/CryptoAES.h"
#include "DLMS/Core.h"

namespace DLMS
{


Core::Core() :
    AARQ(new SAARQ(*this)),
    Action(new SAction(*this)),
    GET(new SGET(*this)),
    SET(new SSET(*this)) {
    this->status = ConnectionStatus::NO;

};


ConnectionStatus Core::Open(const Setting &dlmsSetting,
                            const HDLC::IOSetting &ioSetting,
                            quint8 clientAddr, quint16 srvLogicalAddr, quint16 srvPhysicalAddr) {
    this->initDlmsSettings = dlmsSetting;
    this->usingDlmsSettings = dlmsSetting;
    this->clientAddr = clientAddr;
    this->srvLogicalAddr = srvLogicalAddr;
    this->srvPhysicalAddr = srvPhysicalAddr;

    HDLC::Error hdlcErr = hdlcClient.Open(ioSetting, clientAddr, srvLogicalAddr, srvPhysicalAddr);

    if (HDLC::Error::Ok != hdlcErr) {
        throw static_cast<Error>((quint8)hdlcErr << 8);
    }
    this->status = ConnectionStatus::HDLC_ESTABLISHED;

    this->AARQ->Querry(this->initDlmsSettings, this->usingDlmsSettings);
    if (ConnectionStatus::DLMS_PENDING == this->status) {
        unsigned char buff[1024] = {0};
        Crypto::AES_encryptECB((const unsigned char*)usingDlmsSettings.sToC.constData(), (const unsigned char*)usingDlmsSettings.authenticationKey.toStdString().c_str(), buff);
        Variant sToC = QByteArray::fromRawData((const char*)buff, usingDlmsSettings.sToC.size());
        Variant cToS;
        this->Action->Query({0,0,40,0,0,255}, 1, ClassType::ASSOCIATION_LN, sToC, cToS);

        Crypto::AES_decryptECB((const unsigned char*)cToS.Data().constData(), (const unsigned char*)usingDlmsSettings.authenticationKey.toStdString().c_str(), buff);
        if (QByteArray::fromRawData((const char*)buff, usingDlmsSettings.sToC.size()) == usingDlmsSettings.cToS) {
            this->status = ConnectionStatus::DLMS_ESTABLISHED;
        }
    }
    return this->status;
}
void Core::Close() {
    if (ConnectionStatus::NO < status) {
        hdlcClient.Close();
        status = ConnectionStatus::NO;
    }

}
void Core::Transfer(const QByteArray &txData, QByteArray &rxData) {
    if (ConnectionStatus::HDLC_ESTABLISHED > this->status) {
        throw Error::HDLCConnectionNotOpen;
    }

    QByteArray passData = QByteArray::fromHex(QString("E6 E6 00").toLatin1()) + txData;
    HDLC::Error err = hdlcClient.Transfer(passData, rxData);
    if (HDLC::Error::Ok != err) {
        throw static_cast<Error>((quint8)err << 8);
    }
    if (3 > rxData.size()) {
        throw Error::InvalidSize;
    }
    if ((char)0xE6 != rxData[0] || (char)0xE7 != rxData[1] || (char)0x00 != rxData[2]) {
        throw Error::InvalidDirection;
    }
    if ((char)Command::EXCEPTION_RESPONSE == rxData[3]) {
        switch (rxData[4]) {
        case 0x01:
            throw Error::DLMSConnectionNotOpen;
        default:
            throw Error::ExceptionUnknown;
        }
    }
    rxData = rxData.right(rxData.size() - 3);
}

ConnectionStatus Core::GetConnectionStatus() noexcept {
    if (ConnectionStatus::NO == status) {
        return status;
    }
    try {
        Variant res;
        GET->Query({0, 0, 40, 0, 0, 255}, 8, ClassType::ASSOCIATION_LN, Variant(), res);

        switch (res.toNumber()) {
        case 0:
            this->status = ConnectionStatus::NO;
            break;
        case 1:
            this->status = ConnectionStatus::DLMS_PENDING;
            break;
        case 2:
            this->status = ConnectionStatus::DLMS_ESTABLISHED;
            break;
        default:
            this->status = ConnectionStatus::HDLC_ESTABLISHED;
            break;
        }
    }  catch (HDLC::Error) {
        this->status = ConnectionStatus::NO;
    }  catch (DLMS::Error) {
        this->status = ConnectionStatus::HDLC_ESTABLISHED;
    }
    return this->status;

}

HDLC::HDLCCore& Core::GetHDLC() {
    return hdlcClient;
}

Core::~Core() {
    if (nullptr != AARQ) {
        delete AARQ;
    }
    if (nullptr != Action) {
        delete Action;
    }
    if (nullptr != GET) {
        delete GET;
    }
    if (nullptr != SET) {
        delete SET;
    }

    Close();
}


}
