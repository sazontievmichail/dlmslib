#ifndef DLMSENUMS_H
#define DLMSENUMS_H

#include <QTypeInfo>

namespace DLMS {

/**
 * @brief   Перечисление возможных состояний подключения
 */
enum class ConnectionStatus : quint8 {
    NO                  = 0x00,     // Соединение не установлено
    HDLC_ESTABLISHED    = 0x01,     // Соединение устонавлено по HDLC протоколу
    DLMS_PENDING        = 0x02,     // Соединение установлено чатично, если используетс высокий уровень доступа
    DLMS_ESTABLISHED    = 0x03      // Соединение установлено
};

/**
 * @brief   Перечисление поддерживаемых типв данных DLMS
 */
enum class DataType : quint8 {
    NONE                 = 0x00,
    ARRAY                = 0x01,
    STRUCTURE            = 0x02,
    BOOLEAN              = 0x03,
    BIT_STRING           = 0x04,
    INT32                = 0x05,
    UINT32               = 0x06,
    OCTET_STRING         = 0x09,
    STRING               = 0x0A,
    STRING_UTF8          = 0x0C,
    BINARY_CODED_DESIMAL = 0x0D,
    INT8                 = 0x0F,
    INT16                = 0x10,
    UINT8                = 0x11,
    UINT16               = 0x12,
    COMPACT_ARRAY        = 0x13,
    INT64                = 0x14,
    UINT64               = 0x15,
    ENUM                 = 0x16,
    FLOAT32              = 0x17,
    FLOAT64              = 0x18,
    DATETIME             = 0x19,
    DATE                 = 0x1A,
    TIME                 = 0x1B,
    UNDERFINED           = 255
};

/**
 * @brief   Перечисление возможных классов объектов DLMS
 */
enum class ClassType : quint8 {
    NONE                   = 0,
    DATA                   = 1,  // Данные. Хранение данных
    REGISTER               = 3,  // Регистр. Хранение данных
    EXTENDED_REGISTER      = 4,  // Расширенный регистр. Хранение данных
    DEMAND_REGISTER        = 5,  // Регистр усреднения. Хранение данных
    REGISTER_ACTIVATION    = 6,  // Активируемый регистр. Хранение данных
    PROFILE_GENERIC        = 7,  // Профиль универсальный. Хранение данных
    CLOCK                  = 8,  // Время. Тарификация и фиксация событий
    SCRIPT_TABLE           = 9,  // Таблица сценариев. Тарификация и фиксация событий
    SCHEDULE               = 10, // Расписание. Тарификация и фиксация событий
    SPECIAL_DAYS_TABLE     = 11, // Таблица особых дней. Тарификация и фиксация событий
    ASSOCIATION_SN         = 12, // Не поддерживаетс
    ASSOCIATION_LN         = 15, // Соединение по логическому имени. Управление доступом к данным
    SAP_ASSIGNMENT         = 17, // Назначение точки доступа. Управление доступом к данным
    IMAGE_TRANSFER         = 18, // Передача двоичных блоков. Обновление прошивки
    LOCAL_PORT_SETUP       = 19, // Настройки оптопорта. Интерфейс
    ACTIVITY_CALENDAR      = 20, // Календарь активирования. Тарификация
    REGISTER_MONITOR       = 21, // Регистр контроля. Управление
    SINGLE_ACTION_SCHEDULE = 22, // Регламент одного действия. Фиксация событий
    HDLC_SETUP             = 23, // Настройки HDLC. Интерфейс
    PUSH_SETUP             = 40, // Настройки инициативного выхода. Интерфейс
    SECURITY_SETUP         = 64, // Настройки безопасности. Контроль доступа
    DISCONNECT_CONTROL     = 70, // Управление отключением. Отключение абонента
    LIMITER                = 71  // Ограничитель. Отключение абонента
} ;

/**
 * @brief   Перечисление возможных алгоритмов аутентификации
 */
enum class Authentication : quint8 {
    NONE        = 0x00,
    LOW         = 0x01,
    HIGH        = 0x02,
    /*
     * Не поддерживаются!
     */
    HIGH_MD5,
    HIGH_SHA1,
    HIGH_GMAC,
    HIGH_SHA256,
    HIGH_ECDSA
};

/**
 * @brief   Перечисление флагов отвечающих за поддерживаемый функционал
 */
enum class Conformance {
    NONE                                = 0x00,         // b0000 0000 0000 0000 Reserved zero conformance bit.
    RESERVED_ZERO                       = 0x01,         // b0000 0000 0000 0001 Reserved zero conformance bit.
    GENERAL_PROTECTION                  = 0x02,         // b0000 0000 0000 0010 General protection conformance bit.
    GENERAL_BLOCK_TRANSFER              = 0x04,         // b0000 0000 0000 0100 General block transfer conformance bit.
    READ                                = 0x08,         // b0000 0000 0000 1000 Read conformance bit.
    WRITE                               = 0x10,         // b0000 0000 0001 0000 Write conformance bit.
    UN_CONFIRMED_WRITE                  = 0x20,         // b0000 0000 0010 0000 Un confirmed write conformance bit.
    DELTA_VALUE_ENCODING                = 0x40,         // b0000 0000 0100 0000 Delta value encoding.
    RESERVED_SEVEN                      = 0x80,         // b0000 0000 1000 0000 Reserved seven conformance bit.
    ATTRIBUTE_0_SUPPORTED_WITH_SET      = 0x0100,       // b0000 0001 0000 0000 Attribute 0 supported with set conformance bit.
    PRIORITY_MGMT_SUPPORTED             = 0x0200,       // b0000 0010 0000 0000 Priority mgmt supported conformance bit.
    ATTRIBUTE_0_SUPPORTED_WITH_GET      = 0x0400,       // b0000 0100 0000 0000 Attribute 0 supported with get conformance bit.
    BLOCK_TRANSFER_WITH_GET_OR_READ     = 0x0800,       // b0000 1000 0000 0000 Block transfer with get or read conformance bit.
    BLOCK_TRANSFER_WITH_SET_OR_WRITE    = 0x1000,       // b0001 0000 0000 0000 Block transfer with set or write conformance bit.
    BLOCK_TRANSFER_WITH_ACTION          = 0x2000,       // b0010 0000 0000 0000 Block transfer with action conformance bit.
    MULTIPLE_REFERENCES                 = 0x4000,       // b0100 0000 0000 0000 multiple references conformance bit.
    INFORMATION_REPORT                  = 0x8000,       // b1000 0000 0000 0000 Information report conformance bit.
    DATA_NOTIFICATION                   = 0x010000,     // Data notification conformance bit.
    ACCESS                              = 0x020000,     // Access conformance bit.
    PARAMETERIZED_ACCESS                = 0x040000,     // Parameterized access conformance bit.
    ActioGet                            = 0x080000,     // Get conformance bit.
    SET                                 = 0x100000,     // Set conformance bit
    SELECTIVE_ACCESS                    = 0x200000,     // Selective access conformance bit.
    EVENT_NOTIFICATION                  = 0x400000,     // Event notification conformance bit.
    ACTION                              = 0x800000      // Action conformance bit.
};

/**
 * @brief   перечисление комнд сервисов DLMS
 */
enum class Command : quint8 {
    NONE                       = 0x00,     // No command to execute.
    INITIATE_REQUEST           = 0x01,     // Initiate request.
    INITIATE_RESPONSE          = 0x08,     // Initiate response.
    READ_REQUEST               = 0x05,     // Read request.
    READ_RESPONSE              = 0x0C,     // Read response.
    WRITE_REQUEST              = 0x06,     // Write request.
    WRITE_RESPONSE             = 0x0D,     // Write response.
    GET_REQUEST                = 0xC0,     // Get request.
    GET_RESPONSE               = 0xC4,     // Get response.
    SET_REQUEST                = 0xC1,     // Set request.
    SET_RESPONSE               = 0xC5,     // Set response.
    METHOD_REQUEST             = 0xC3,     // Action request.
    METHOD_RESPONSE            = 0xC7,     // Action response.
    DISCONNECT_MODE            = 0x1f,     // HDLC Disconnect Mode. Responder for Disconnect Mode request.
    UNACCEPTABLE_FRAME         = 0x97,     // HDLC Unacceptable Frame.

    SNRM                       = 0x93,     // SNRM request.
    UA                         = 0x73,     // UA request.

    AARQ                       = 0x60,     // AARQ request.
    AARE                       = 0x61,     // AARE request.

    DISCONNECT_REQUEST         = 0x53,     // Disconnect request for HDLC framing.
    RELEASE_REQUEST            = 0x62,     // Release request.
    RELEASE_RESPONSE           = 0x63,     // Release response.
    CONFIRMED_SERVICE_ERROR    = 0x0E,     // Confirmed Service Error.
    EXCEPTION_RESPONSE         = 0xD8,     // Exception Response.
    GENERAL_BLOCK_TRANSFER     = 0xE0,     // General Block Transfer.
    ACCESS_REQUEST             = 0xD9,     // Access Request.
    ACCESS_RESPONSE            = 0xDA,     // Access Response.
    DATA_NOTIFICATION          = 0x0F,     // Data Notification request.

    GLO_GET_REQUEST                    = 0xC8,     // Glo get request.
    GLO_GET_RESPONSE                   = 0xCC,     // Glo get response.
    GLO_SET_REQUEST                    = 0xC9,     // Glo set request.
    GLO_SET_RESPONSE                   = 0xCD,     // Glo set response.
    GLO_GENERAL_CIPHERING              = 0xDB,     // Glo general ciphering.
    GLO_EVENT_NOTIFICATION_REQUEST     = 0xCA,     // Glo event notification request.
    GLO_METHOD_REQUEST                 = 0xCB,     // Glo method request.
    GLO_METHOD_RESPONSE                = 0xCF,     // Glo method response.
    GLO_INITIATE_REQUEST               = 0x21,     // Glo Initiate request.
    GLO_READ_REQUEST                   = 37,       // Glo read request.
    GLO_WRITE_REQUEST                  = 38,       // Glo write request.
    GLO_INITIATE_RESPONSE              = 40,       // Glo Initiate response.
    GLO_READ_RESPONSE                  = 44,       // Glo read response.
    GLO_WRITE_RESPONSE                 = 45,       // Glo write response.
    GLO_CONFIRMED_SERVICE_ERROR        = 46,       // GLO confirmed service error.
    GENERAL_GLO_CIPHERING              = 0xDB,     // General GLO ciphering.

    GENERAL_DED_CIPHERING              = 0xDC,     // General DED ciphering.
    GENERAL_CIPHERING                  = 0xDD,     // General ciphering.
    INFORMATION_REPORT                 = 0x18,     // Information Report request.
    EVENT_NOTIFICATION                 = 0xC2,     // Event Notification request.

    DED_GET_REQUEST                    = 0xD0,     // Ded get request.
    DED_GET_RESPONSE                   = 0xD4,     // Ded get response.
    DED_SET_REQUEST                    = 0xD1,     // Ded set request.
    DED_SET_RESPONSE                   = 0xD5,     // Ded set response.
    DED_EVENT_NOTIFICATION             = 0xD2,     // Ded event notification request.
    DED_METHOD_REQUEST                 = 0xD3,     // Ded method request.
    DED_METHOD_RESPONSE                = 0xD7,     // Ded method response.
    DED_READ_REQUEST                   = 69,       // Ded read request.
    DED_WRITE_REQUEST                  = 70,       // Ded write request.
    DED_READ_RESPONSE                  = 76,       // Ded read response.
    DED_WRITE_RESPONSE                 = 77,       // Ded write response.
    DED_CONFIRMED_SERVICE_ERROR        = 78,       // Ded confirmed service error.

    GATEWAY_REQUEST                    = 0xE6,     // Request message from client to gateway.
    GATEWAY_RESPONSE                   = 0xE7,     // Response message from gateway to client.

    DED_INITIATE_REQUEST               = 65,       // Ded initiate request.
    DED_INITIATE_RESPONSE              = 72,       // Ded initiate response.

    DISCOVER_REQUEST                   = 0x1D,     // PLC discover request.
    DISCOVER_REPORT                    = 0x1E,     // PLC discover report.
    REGISTER_REQUEST                   = 0x1C,     // PLC register request.
    PING_REQUEST                       = 0x19,     // PLC ping request.
    PING_RESPONSE                      = 0x1A,     // PLC ping response.
    REPEAT_CALL_REQUEST                = 0x1F      // PLC repeat call request.
};

/**
 * @brief   Перечисление возможных результатов ошибок диагностики на приборе учета
 */
enum class SourceDiagnostic : quint8 {
    NONE                                                            = 0x00,
    NO_REASON_GIVEN                                                 = 0x01,
    APPLICATION_CONTEXT_NAME_NOT_SUPPORTED                          = 0x02,
    APPLICATION_CALLING_AP_TITLE_NOT_RECOGNIZED                     = 0x03,
    APPLICATION_CALLING_AP_INVOCATION_IDENTIFIER_NOT_RECOGNIZED     = 0x04,
    APPLICATION_CALLING_AE_QUALIFIER_NOT_RECOGNIZED                 = 0x05,
    APPLICATION_CALLING_AE_INVOCATION_IDENTIFIER_NOT_RECOGNIZED     = 0x06,
    APPLICATION_CALLED_AP_TITLE_NOT_RECOGNIZED                      = 0x07,
    APPLICATION_CALLED_AP_INVOCATION_IDENTIFIER_NOT_RECOGNIZED      = 0x08,
    APPLICATION_CALLED_AE_QUALIFIER_NOT_RECOGNIZED                  = 0x09,
    APPLICATION_CALLED_AE_INVOCATION_IDENTIFIER_NOT_RECOGNIZED      = 0x0A,
    AUTHENTICATION_MECHANISM_NAME_NOT_RECOGNISED                    = 0x0B,
    AUTHENTICATION_MECHANISM_NAME_REQUIRED                          = 0x0C,
    AUTHENTICATION_FAILURE                                          = 0x0D,
    AUTHENTICATION_REQUIRED                                         = 0x0E,
};

/**
 * @brief   Перечисление возможных результатов диагностики
 */
enum class AssotiationResult : quint8 {
    DLMS_ASSOCIATION_RESULT_ACCEPTED                = 0x00,
    DLMS_ASSOCIATION_RESULT_PERMANENT_REJECTED      = 0x01,
    DLMS_ASSOCIATION_RESULT_TRANSIENT_REJECTED      = 0x02
};

/**
 * @brief   Перечисление возможных ошибок запроса поддерживаемых сервисов
 */
enum class ConfirmedServiceError : quint8 {
    NONE                = 0x00,
    INITIATE_ERROR      = 0x01,
    ERROR_READ          = 0x05,
    ERROR_WRITE         = 0x06
};

/**
 * @brief   Перечисление возможных ошибок при установлении связи
 */
enum class ServiceError : quint8 {
    NONE                     = 0xff,
    APPLICATION_REFERENCE    = 0x00,
    HARDWARE_RESOURCE        = 0x01,
    VDE_STATE_ERROR          = 0x02,
    SERVICE                  = 0x03,
    DEFINITION               = 0x04,
    ACCESS                   = 0x05,
    INITIATE                 = 0x06,
    LOAD_DATASET             = 0x07,
    TASK                     = 0x08,
    OTHER_ERROR              = 0x09
};

/**
 * @brief   Структура с информацией и результатами диагностики при установлении связи
 */
struct AuthenticationResult {
    SourceDiagnostic sourceDiagnostic;
    AssotiationResult  assotiationResult;
    ServiceError serviceError;
    ConfirmedServiceError confirmedServiceError;
};

}

#endif // DLMSENUMS_H
