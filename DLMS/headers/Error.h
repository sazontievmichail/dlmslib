#ifndef DLMSERROR_H
#define DLMSERROR_H

#include <QTypeInfo>

namespace DLMS {

//@TODO: добавить мапу с описанием ошибок и сделать функцию getErrorDescription
/**
 * @brief The Error enum ошибки генерируемые DLMS модулем
 */
enum class Error : quint16 {
    Ok                                  = 0,
    InvalidData,
    InvalidMethod,
    InvalidSize,
    InvalidTag,
    InvalidType,
    InvalidDirection,
    InvalidSecurityMechanism,
    HDLCConnectionNotOpen,
    DLMSConnectionNotOpen,
    UnsupportedType,
    UnsupportedMethod,
    UnsupportedAuthenticationMethod,
    ExceptionUnknown,
    OutOfMemory,
};

}
#endif // DLMSERROR_H
