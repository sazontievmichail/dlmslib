#ifndef ISERVICE_H
#define ISERVICE_H

#include "SelectiveAccess.h"
#include "Variant.h"
#include "Obis.h"

namespace DLMS {

class Core;

class IService {
public:
    IService(Core &core);
    IService(IService &service);
    virtual ~IService() = 0;

    virtual quint8 query(const Obis &dlmsObj
                         , const quint8 method
                         , const ClassType classType
                         , const Variant &data, Variant &result
                         , SelectiveAccess::Type selectiveAccess = SelectiveAccess::Type::NONE) = 0;

protected:
    virtual QByteArray handleRequest(const Obis &dlmsObj, const quint8 attribute, const ClassType classType, const Variant &data, SelectiveAccess::Type selectiveAccess) = 0;
    virtual quint8 handleResponse(const QByteArray &data, Variant &result) = 0;

protected:
    Core *core;
};

}

#endif // ISERVICE_H
