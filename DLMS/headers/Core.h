#ifndef DLMS_H
#define DLMS_H

#include "Enums.h"
#include "Error.h"
#include "Setting.h"
#include "IService.h"
#include "HDLC/headers/Core.h"

namespace DLMS {

class SAARQ;

class Core {
    enum MLRDLMSConnectionStatus {
        CONNECTION_STATUS_NO            = 0,
        CONNECTION_STATUS_PENDING       = 1,
        CONNECTION_STATUS_ESTABLISHED   = 2
    } ;

public:
    /**
     * @author      Michail Sazontev
     * @brief       Constructer
     */
    Core();
    ~Core();

    /**
     * @author      Michail Sazontev
     * @brief       Function return connection status
     *
     * @return      return connecton status
     */
    ConnectionStatus getConnectionStatus() noexcept;
    /**
     * @author      Michail Sazontev
     * @brief       Function return current hdlc connection
     *
     * @return      return hdlc client
     */
    HDLC::Core& getHDLC();

    /**
     * @author      Michail Sazontev
     * @brief       Function initialie hdlc connection and open protected dlms connection
     *
     * @param[in]   dlmsSetting         The DLMS settings
     * @param[in]   ioSetting           The input/output setting
     * @param[in]   clientAddr          The client address 0x10 - public 0x20 - reader 0x30 - configurator
     * @param[in]   srvLogicalAddr      The server logical address
     * @param[in]   srvPhysicalAddr     The server physycal address
     *
     * @return      Connection status
     */
    ConnectionStatus open(const Setting &dlmsSetting,
                          const HDLC::IOSetting &ioSetting, quint8 clientAddr, quint16 srvLogicalAddr, quint16 srvPhysicalAddr);
    /**
     * @author      Michail Sazontev
     * @brief       Function close hdlc connection
     */
    void close();

protected:
    /**
     * @brief       Sending data and receiving a response
     * @author      Michail Sazontev
     *
     * @param[in]   txData      The data to transmit
     * @param[out]  rxData      The data to receive
     * @protected
     */
    void transfer(const QByteArray &txData, QByteArray &rxData);

    // The class has a bi-directional relationship with the service classes
    friend class SAARQ;
    friend class SAction;
    friend class SGET;
    friend class SSET;

public:
    // Public services
    IService *const Action;
    IService *const GET;
    IService *const SET;

protected:
    Setting initDlmsSettings;
    Setting usingDlmsSettings;

    ConnectionStatus status;

private:
    quint8 clientAddr;
    quint16 srvLogicalAddr;
    quint16 srvPhysicalAddr;

    HDLC::Core hdlcClient;

    // Private service required to establish a connection
    SAARQ *AARQ;
};



}

#endif // DLMS_H
