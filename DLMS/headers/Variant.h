#ifndef DLMSVARIANT_H
#define DLMSVARIANT_H

#include "Enums.h"
#include "Error.h"
#include "Obis.h"
#include <QMetaType>
#include <QDateTime>

namespace DLMS {

/**
 * @author  Michail Sazontev
 * @brief   Универсальный тип данных используемый в ходе обмена данными между клиентом и сервером
 *          Используется для упаковки базовых типов в модель XDR и обратно
 */
class Variant {
public:
    Variant();
    Variant(bool value);
    Variant(float value);
    Variant(double value);
    Variant(quint8 value);
    Variant(quint16 value);
    Variant(quint32 value);
    Variant(quint64 value);
    Variant(qint8 value);
    Variant(qint16 value);
    Variant(qint32 value);
    Variant(qint64 value);
    Variant(const QByteArray&);
    Variant(const QVector<Variant>&, bool isStruct = false);
    Variant(const QDateTime dateTime, quint16 deviation = 0xff4c, quint8 status = 0x00);
    Variant(const QDate);
    ~Variant();

    Variant& operator= (bool);
    Variant& operator= (quint8);
    Variant& operator= (quint16);
    Variant& operator= (quint32);
    Variant& operator= (quint64);
    Variant& operator= (qint8);
    Variant& operator= (qint16);
    Variant& operator= (qint32);
    Variant& operator= (qint64);
    Variant& operator= (const QByteArray&);
    Variant& operator= (const QVector<Variant>&);
    const Variant& operator[] (quint32) const;

    bool isNull() const;
    bool isArray() const;
    bool isStruct() const;
    bool isNumber() const;
    bool isBoolean() const;
    bool isOctetString() const;
    bool isObis() const;
    bool isDateTime() const;
    bool isDate() const;
    bool isEnum() const;

    const QByteArray& getData() const;

    qint64      toNumber() const;
    QString     toString() const;
    bool        toBoolean() const;
    QByteArray  toOctetString() const;
    QString     toXML() const;  //@TODO: исползуется только при отладке, в дальнейшем стоит реализовать на осове QDomDocument
    Obis        toObis() const;
    QDateTime   toDateTime() const;
    QDate       toDate() const;
    const QVector<Variant>& toArray() const;

    int size() const;           //@TODO: стоит реализовать итераторы или просто возвращать массив с данными

    QByteArray xdrData() const;

    static Variant parseValue(const QByteArray &data);

    void clear();

private:
    static void checkTagLen(QByteArray::const_iterator &it, const QByteArray &data);
    static Variant parse(QByteArray::const_iterator &it, const QByteArray &data);
    static quint16 getTagLen(QByteArray::const_iterator &it, const QByteArray &data);

private:
    DataType type;
    //@TODO: Возможно стоит использовать единую переменную для хранения чисел или использовать QVariant
    QByteArray data;                // for octet string
    qint64 intVal;                  // for signed
    quint64 uintVal;                // for unsigned
    float floatVal;                 // for float
    double doubleVal;               // for double
    bool boolVal;                   // for bool
    QVector<Variant> listVal;       // for structure or array
};

}
Q_DECLARE_METATYPE(DLMS::Variant);

#endif // DLMSVARIANT_H
