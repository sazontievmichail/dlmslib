#ifndef DLMSSETTING_H
#define DLMSSETTING_H

#include "Enums.h"
#include <QString>

namespace DLMS {

struct Setting {
    Authentication authentication;          // Уровень доступа
    QString authenticationKey;              // Взависимости от механизма шифрования используется как пароль или ключ

    bool useLongNameReferencing;            // Сейчас возможно использоваие только длинных адресов
    quint32 conformance;                    // Битовое поле содержащее поддерживаемые сервисы
    quint8 dlmsVersion;                     // Версия dlms, сейчас возможна только 6
    quint16 maxApduSize;                    // Максимальный размер apdu пакетв
    QByteArray cToS;                        // Случайная последовательность, заполнять не нужно
    QByteArray sToC;                        // Случайная последовательность, заполнять не нужно
    quint8 clientAddr;
};

// Прототип настроек для соединения с сервером без уровня безопасности
const Setting dlmsNSDefaultSetting = {
                                        .authentication = Authentication::NONE,
                                        .authenticationKey = "",
                                        .useLongNameReferencing = true,
                                        .conformance = 0x1E1D, //(DLMSConformance::READ | DLMSConformance::BLOCK_TRANSFER_WITH_GET_OR_READ | DLMSConformance::BLOCK_TRANSFER_WITH_ACTION),
                                        .dlmsVersion = 0x06,
                                        .maxApduSize = 1024,
                                        .cToS = QByteArray(),
                                        .sToC = QByteArray(),
                                        .clientAddr = 0x10 };
// Прототип настроек для соединения с низким уровнем безопасности
const Setting dlmsLLDefaultSetting = {
                                        .authentication = Authentication::LOW,
                                        .authenticationKey = "789456",
                                        .useLongNameReferencing = true,
                                        .conformance = 0x1E1D, //(DLMSConformance::READ | DLMSConformance::BLOCK_TRANSFER_WITH_GET_OR_READ | DLMSConformance::BLOCK_TRANSFER_WITH_ACTION),
                                        .dlmsVersion = 0x06,
                                        .maxApduSize = 1024,
                                        .cToS = QByteArray(),
                                        .sToC = QByteArray(),
                                        .clientAddr = 0x20 };
// Прототип настроек для соединения с высоким уровнем безопасности
const Setting dlmsHLDefaultSetting = {
                                        .authentication = Authentication::HIGH,
                                        .authenticationKey = "1597531234567890",
                                        .useLongNameReferencing = true,
                                        .conformance = 0x1E1D, //(DLMSConformance::READ | DLMSConformance::BLOCK_TRANSFER_WITH_GET_OR_READ | DLMSConformance::BLOCK_TRANSFER_WITH_ACTION),
                                        .dlmsVersion = 0x06,
                                        .maxApduSize = 1024,
                                        .cToS = QByteArray(),
                                        .sToC = QByteArray(),
                                        .clientAddr = 0x30 };

}

#endif // DLMSSETTING_H
